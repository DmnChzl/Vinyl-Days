let zIndex = {};

for (let idx = 0; idx <= 50; idx += 5) {
  zIndex = {
    ...zIndex,
    [idx]: idx + ''
  };
}

export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      animation: {
        'spin-slow': 'spin 5s linear infinite'
      },
      zIndex
    }
  },
  plugins: []
};
