import react from '@vitejs/plugin-react-swc';
import path from 'path';
import { defineConfig } from 'vite';
// import jotaiDebugLabel from 'jotai/babel/plugin-debug-label';
// import jotaiReactRefresh from 'jotai/babel/plugin-react-refresh';
import svgr from 'vite-plugin-svgr';

// https://vitejs.dev/config
export default defineConfig({
  plugins: [
    react({
      plugins: [
        ['@swc-jotai/react-refresh', {}],
        ['@swc-jotai/debug-label', {}]
      ]
    }),
    svgr()
  ],
  test: {
    environment: 'jsdom',
    globals: true,
    setupFiles: './src/setupTests.ts',
    // deps: { inline: [/react/] },
    threads: false,
    isolate: false,
    include: ['src/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],
    coverage: {
      provider: 'v8',
      exclude: [
        'src/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}',
        'src/constants',
        'src/models',
        'src/setupTests.ts'
      ],
      reporter: ['text', 'lcov']
    },
    testTimeout: 15000,
    reporters: ['default']
  },
  resolve: {
    alias: {
      '@': path.resolve('./src'),
      '~': path.resolve('./src')
    },
    conditions: ['development', 'browser']
  },
  server: {
    port: 1234
  }
});
