import { useAtomValue, useSetAtom } from 'jotai';
import { ReactNode, useEffect } from 'react';
import { createPortal } from 'react-dom';
import { CloseIcon } from './assets/icons';
import { modalVisibilityAtom } from './atoms';

interface Props {
  children: ReactNode;
}

function Modal({ children }: Props) {
  const setModalVisibility = useSetAtom(modalVisibilityAtom);

  useEffect(() => {
    return () => {
      setModalVisibility(false);
    };
  }, []);

  return (
    <div className="fixed inset-0 z-20 flex h-full w-full items-center justify-center bg-[#00000040]">
      <div className="relative mx-4 flex w-full flex-col space-y-4 rounded-lg bg-white p-4 shadow sm:w-[512px]">
        {children}

        <button
          className="absolute right-4 top-0 rounded-full p-2 text-neutral-700 hover:bg-neutral-100 hover:text-red-600"
          type="button"
          onClick={() => setModalVisibility(false)}
          aria-label="Close Modal">
          <CloseIcon aria-hidden />
        </button>
      </div>
    </div>
  );
}

type PortalProps = { container?: HTMLElement } & Props;

export default function ModalPortal({ container = document.body, ...props }: PortalProps) {
  const modalVisibility = useAtomValue(modalVisibilityAtom);

  if (!modalVisibility) return null;
  return createPortal(<Modal {...props} />, container);
}
