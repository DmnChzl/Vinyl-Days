import ReactDOM from 'react-dom/client';
import App from './App';
// import { worker } from './__mocks__/browser.ts';
import '@fontsource/rubik/400.css';
import '@fontsource/rubik/700.css';
import './index.css';

if (import.meta.env['MODE'] === 'development') {
  // worker.start();
}

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(<App />);
