import clsx from 'clsx';
import { useAtomValue } from 'jotai';
import { createPortal } from 'react-dom';
import { BellActiveIcon, CloseIcon } from './assets/icons';
import { isNotificationQueueEmptyAtom } from './atoms';
import { NOTIFICATION_THEME } from './constants';
import useNotificationQueueAtom from './hooks/useNotificationQueueAtom';

function NotificationQueue() {
  const [{ notificationQueue }, { delNotification }] = useNotificationQueueAtom();
  const [notification] = notificationQueue;

  return (
    <div className="fixed bottom-0 left-0 z-30 w-full">
      <div
        className={clsx(
          '-shadow-sm mx-auto flex w-full items-center space-x-4 rounded-t-[16px] p-4 sm:w-[512px]',
          notification?.theme === NOTIFICATION_THEME.LIGHT && 'bg-white',
          notification?.theme === NOTIFICATION_THEME.BLUE && 'bg-blue-600',
          notification?.theme === NOTIFICATION_THEME.RED && 'bg-red-600',
          notification?.theme === NOTIFICATION_THEME.DARK && 'bg-neutral-900'
        )}>
        <BellActiveIcon
          className={clsx(
            notification?.theme === NOTIFICATION_THEME.LIGHT && 'text-neutral-500',
            [NOTIFICATION_THEME.BLUE, NOTIFICATION_THEME.RED].includes(notification?.theme) && 'text-white',
            notification?.theme === NOTIFICATION_THEME.DARK && 'text-neutral-300'
          )}
          aria-hidden
        />
        <div className="flex flex-grow flex-col">
          <strong
            className={clsx(
              notification?.theme === NOTIFICATION_THEME.LIGHT && 'text-neutral-900',
              [NOTIFICATION_THEME.BLUE, NOTIFICATION_THEME.RED].includes(notification?.theme) && 'text-white',
              notification?.theme === NOTIFICATION_THEME.DARK && 'text-neutral-100'
            )}>
            {notification?.title}
          </strong>
          {notification?.subTitle && (
            <span
              className={clsx(
                'text-sm',
                notification?.theme === NOTIFICATION_THEME.LIGHT && 'text-neutral-500',
                [NOTIFICATION_THEME.BLUE, NOTIFICATION_THEME.RED].includes(notification?.theme) && 'text-white',
                notification?.theme === NOTIFICATION_THEME.DARK && 'text-neutral-300'
              )}>
              {notification.subTitle}
            </span>
          )}
        </div>
        <button
          className={clsx(
            'rounded-full p-2 ',
            notification?.theme === NOTIFICATION_THEME.LIGHT && 'hover:bg-neutral-100',
            notification?.theme === NOTIFICATION_THEME.BLUE && 'hover:bg-blue-500',
            notification?.theme === NOTIFICATION_THEME.RED && 'hover:bg-red-500',
            notification?.theme === NOTIFICATION_THEME.DARK && 'hover:bg-neutral-800'
          )}
          type="button"
          onClick={() => delNotification(notification?.id)}
          aria-label="Close">
          <CloseIcon
            className={clsx(
              notification?.theme === NOTIFICATION_THEME.LIGHT && 'text-neutral-500',
              [NOTIFICATION_THEME.BLUE, NOTIFICATION_THEME.RED].includes(notification?.theme) && 'text-white',
              notification?.theme === NOTIFICATION_THEME.DARK && 'text-neutral-300'
            )}
            aria-hidden
          />
        </button>
      </div>
    </div>
  );
}

interface Props {
  container?: HTMLElement;
}

export default function NotificationPortal({ container = document.body }: Props) {
  const isNotificationQueueEmpty = useAtomValue(isNotificationQueueEmptyAtom);

  if (isNotificationQueueEmpty) return null;
  return createPortal(<NotificationQueue />, container);
}
