export const APP_NAME = 'Vinyl Days App';
export const APP_STATE_KEY = 'vinyl_days_app';

export const DEMO = ['Black Sands', 'The North Borders', 'Migration'];

export const NOTIFICATION_THEME: Record<string, string> = {
  LIGHT: 'light',
  BLUE: 'blue',
  RED: 'red',
  DARK: 'dark'
};

export const PLAY_STATE: Record<string, string> = {
  RUNNING: 'running',
  PAUSED: 'paused'
};

export const VIEW_TYPE: Record<string, string> = {
  GRID: 'grid',
  LIST: 'list'
};
