export const APPLICATION_DATA_INFO_MSG = 'Download the application data, then upload it later';
export const CORRUPTED_FILE_ERROR_MSG = 'It seems that the file is corrupted...';

export const ACCOUNT_CLIENT_INFO_MSG = 'Check your account and set the client first';
export const ACCOUNT_SPOTIFY_INFO_MSG = 'Check your account and connect to Spotify first';

export const FETCH_TOKEN_ERROR_MSG = 'Something wrong happened while fetching new token...';
export const FETCH_SEARCH_ERROR_MSG = 'Something wrong happened while submitting the search...';
export const FETCH_TRACK_ERROR_MSG = 'Something wrong happened while fetching track...';
export const FETCH_ALBUM_ERROR_MSG = 'Something wrong happened while fetching album...';

export const getFavoriteLikeMsg = (track: string): string => `${track} added to favorite`;
export const getFavoriteUnlikeMsg = (track: string): string => `${track} removed from favorite`;
