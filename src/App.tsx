import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import NotificationPortal from './NotificationProvider';
import useAppStateEffect from './hooks/useAppStateEffect';
import useSpotifyConnect from './hooks/useSpotifyConnect';
import useSpotifyPlayer from './hooks/useSpotifyPlayer';
import Account from './pages/Account';
import CurrentAlbum from './pages/CurrentAlbum';
import Home from './pages/Home';
import NowPlaying from './pages/NowPlaying';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />
  },
  {
    path: '/account',
    element: <Account />
  },
  {
    path: '/album/:albumId',
    element: <CurrentAlbum />
  },
  {
    path: '/playing/:trackId',
    element: <NowPlaying />
  },
  {
    path: '*',
    element: <Home />
  }
]);

export default function App() {
  const initialized = useAppStateEffect();

  useSpotifyConnect();
  useSpotifyPlayer();

  return (
    <div id="app">
      <NotificationPortal />
      {initialized && <RouterProvider router={router} />}
    </div>
  );
}
