export interface LikedTrack {
  id: string;
  name: string;
  artist: string;
  coverUrl: string;
  timestamp?: number;
}
