import type { Artist } from './Artist';
import type { Image } from './Image';

export interface Track {
  album: {
    artists: Artist[];
    id: string;
    images: Image[];
    name: string;
    release_date: string;
    total_tracks: number;
  };
  artists: Artist[];
  duration_ms: number;
  id: string;
  name: string;
  track_number: number;
  uri: string;
}
