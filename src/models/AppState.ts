import { LikedTrack } from './LikedTrack';
import { SearchedAlbum } from './SearchedAlbum';

export interface AppState {
  createdAt: number;
  modifiedAt: number;
  clientId: string;
  clientSecret: string;
  accessToken: string;
  accessTokenExpiration: number;
  refreshToken?: string;
  searchedAlbums: SearchedAlbum[];
  likedTracks: LikedTrack[];
}

export const instanceOfAppState = (obj: Partial<AppState>): boolean => {
  const KEYS = [
    'createdAt',
    'modifiedAt',
    'clientId',
    'clientSecret',
    'accessToken',
    'accessTokenExpiration',
    'refreshToken',
    'searchedAlbums',
    'likedTracks'
  ];

  return Object.keys(obj).every(key => KEYS.includes(key));
};
