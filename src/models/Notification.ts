export interface Notification {
  id: string;
  title: string;
  subTitle?: string;
  theme: string;
}
