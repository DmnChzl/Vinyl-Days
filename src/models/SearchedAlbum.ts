export interface SearchedAlbum {
  id: string;
  name: string;
  count: number;
  timestamp?: number;
}
