import type { Artist } from './Artist';
import type { Image } from './Image';

export interface Album {
  artists: Artist[];
  id: string;
  images: Image[];
  name: string;
  release_date: string;
  total_tracks: number;
  tracks: {
    items: {
      artists: Artist[];
      duration_ms: number;
      id: string;
      name: string;
      track_number: number;
    }[];
  };
}
