interface GetOptions {
  parseJson?: boolean;
  useLocal?: boolean;
}

/**
 * @method getItem
 * @param {string} key
 * @param {GetOptions} options { parseJson, useLocal }
 * @returns {*} Item
 */
export const getItem = <T>(key: string, options: Partial<GetOptions> = {}): Partial<T> | string => {
  const { parseJson = true, useLocal = false } = options;

  const storage: Storage = useLocal ? localStorage : sessionStorage;
  const item = storage.getItem(key) || '{}';

  if (parseJson) {
    try {
      return JSON.parse(item);
    } catch {
      // ! [ object Object ]
      return {};
    }
  }

  return item;
};

interface SetOptions {
  forceUpdate?: boolean;
  useLocal?: boolean;
}

/**
 * @method setItem
 * @param {string} key
 * @param {*} value
 * @param {SetOptions} options { forceUpdate, useLocal }
 */
export const setItem = <T>(key: string, value: Partial<T>, options: SetOptions = {}) => {
  const { forceUpdate = false, useLocal = false } = options;

  const storage: Storage = useLocal ? localStorage : sessionStorage;
  const prevItem = getItem<T>(key, { parseJson: true, useLocal }) as Partial<T>;
  const nextItem = forceUpdate ? value : { ...prevItem, ...value };

  storage.setItem(key, JSON.stringify(nextItem));
};
