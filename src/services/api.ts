const api = {
  spotify: {
    authorize: (params: string) => `https://accounts.spotify.com/authorize/?${params}`,
    token: () => 'https://accounts.spotify.com/api/token',
    search: (query: string) => `https://api.spotify.com/v1/search?q=album:${query}&type=album`,
    album: (id: string) => `https://api.spotify.com/v1/albums/${id}`,
    track: (id: string) => `https://api.spotify.com/v1/tracks/${id}`,
    player: () => 'https://api.spotify.com/v1/me/player',
    queue: (uri: string) => `https://api.spotify.com/v1/me/player/queue?uri=${uri}`
  }
};

export default api;
