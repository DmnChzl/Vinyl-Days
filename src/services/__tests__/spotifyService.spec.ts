import { rest } from 'msw';
import { setupServer } from 'msw/node';
import * as SpotifyService from '../spotifyService';

describe('SpotifyService', () => {
  const server = setupServer();

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  describe('grantCredentials', () => {
    it("Shoud Returns 20x 'statusCode'", async () => {
      server.use(
        rest.post('https://accounts.spotify.com/api/token', (req, res, ctx) => {
          return res(
            ctx.json({
              access_token: 'access_token',
              token_type: 'Bearer',
              expires_in: 60000
            })
          );
        })
      );

      const result = await SpotifyService.grantClientCredentials('clientId:clientSecret');
      expect(result.token_type).toEqual('Bearer');
    });

    it("Shoud Returns 40x 'statusCode'", async () => {
      server.use(rest.post('https://accounts.spotify.com/api/token', (_, res, ctx) => res(ctx.status(418))));

      const error = await SpotifyService.grantClientCredentials('clientId:clientSecret').catch(err => err);
      expect(error.statusCode).toEqual(418);
    });

    it("Shoud Returns 50x 'statusCode'", async () => {
      server.use(rest.post('https://accounts.spotify.com/api/token', (_, res, ctx) => res(ctx.status(500))));

      const error = await SpotifyService.grantClientCredentials('clientId:clientSecret').catch(err => err);
      expect(error.statusCode).toEqual(500);
    });
  });

  describe('searchAlbums', () => {
    it("Shoud Returns 20x 'statusCode'", async () => {
      server.use(
        rest.get('https://api.spotify.com/v1/search*', (req, res, ctx) => {
          return res(
            ctx.json({
              albums: {
                items: [
                  {
                    artists: [{ name: 'Artist' }],
                    id: 'abc123',
                    name: 'Album'
                  },
                  {
                    artists: [{ name: 'Artist' }],
                    id: 'def456',
                    name: 'Album'
                  },
                  {
                    artists: [{ name: 'Artist' }],
                    id: 'ghi789',
                    name: 'Album'
                  }
                ]
              }
            })
          );
        })
      );

      const result = await SpotifyService.searchAlbums('query', 'token');
      expect(result.albums.items).toHaveLength(3);
    });

    it("Shoud Returns 40x 'statusCode'", async () => {
      server.use(rest.get('https://api.spotify.com/v1/search*', (_, res, ctx) => res(ctx.status(418))));

      const error = await SpotifyService.searchAlbums('query', 'token').catch(err => err);
      expect(error.statusCode).toEqual(418);
    });

    it("Shoud Returns 50x 'statusCode'", async () => {
      server.use(rest.get('https://api.spotify.com/v1/search*', (_, res, ctx) => res(ctx.status(500))));

      const error = await SpotifyService.searchAlbums('query', 'token').catch(err => err);
      expect(error.statusCode).toEqual(500);
    });
  });

  describe('getAlbum', () => {
    it("Shoud Returns 20x 'statusCode'", async () => {
      server.use(
        rest.get('https://api.spotify.com/v1/albums/abc123', (req, res, ctx) => {
          return res(
            ctx.json({
              artists: [
                {
                  id: 'ghi789',
                  name: 'My Artist'
                }
              ],
              id: 'abc123',
              images: [
                {
                  height: 512,
                  url: 'https://i.scdn.co/image',
                  width: 512
                }
              ],
              name: 'My Album',
              release_date: 'unknown',
              total_tracks: 1,
              tracks: {
                items: [
                  {
                    artists: [
                      {
                        id: 'ghi789',
                        name: 'My Artist'
                      }
                    ],
                    duration_ms: 60000,
                    id: 'def456',
                    name: 'My Track',
                    track_number: 1
                  }
                ]
              }
            })
          );
        })
      );

      const result = await SpotifyService.getAlbum('abc123', 'token');
      expect(result.name).toEqual('My Album');
    });

    it("Shoud Returns 40x 'statusCode'", async () => {
      server.use(rest.get('https://api.spotify.com/v1/albums/abc123', (_, res, ctx) => res(ctx.status(418))));

      const error = await SpotifyService.getAlbum('abc123', 'token').catch(err => err);
      expect(error.statusCode).toEqual(418);
    });

    it("Shoud Returns 50x 'statusCode'", async () => {
      server.use(rest.get('https://api.spotify.com/v1/albums/abc123', (_, res, ctx) => res(ctx.status(500))));

      const error = await SpotifyService.getAlbum('abc123', 'token').catch(err => err);
      expect(error.statusCode).toEqual(500);
    });
  });

  describe('getTrack', () => {
    it("Shoud Returns 20x 'statusCode'", async () => {
      server.use(
        rest.get('https://api.spotify.com/v1/tracks/abc123', (req, res, ctx) => {
          return res(
            ctx.json({
              album: {
                artists: [
                  {
                    id: 'ghi789',
                    name: 'My Artist'
                  }
                ],
                id: 'abc123',
                images: [{ height: 512, url: 'https://i.scdn.co/image', width: 512 }],
                name: 'My Album',
                release_date: 'unknown',
                total_tracks: 1
              },
              artists: [
                {
                  id: 'ghi789',
                  name: 'My Artist'
                }
              ],
              duration_ms: 60000,
              id: 'def456',
              name: 'My Track',
              track_number: 1,
              uri: 'spotify:track:def456'
            })
          );
        })
      );

      const result = await SpotifyService.getTrack('abc123', 'token');
      expect(result.name).toEqual('My Track');
    });

    it("Shoud Returns 40x 'statusCode'", async () => {
      server.use(rest.get('https://api.spotify.com/v1/tracks/abc123', (_, res, ctx) => res(ctx.status(418))));

      const error = await SpotifyService.getTrack('abc123', 'token').catch(err => err);
      expect(error.statusCode).toEqual(418);
    });

    it("Shoud Returns 50x 'statusCode'", async () => {
      server.use(rest.get('https://api.spotify.com/v1/tracks/abc123', (_, res, ctx) => res(ctx.status(500))));

      const error = await SpotifyService.getTrack('abc123', 'token').catch(err => err);
      expect(error.statusCode).toEqual(500);
    });
  });
});
