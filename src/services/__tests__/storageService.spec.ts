import StorageMock from '@/__mocks__/StorageMock';
import * as StorageService from '../storageService';

describe('StorageService', () => {
  beforeAll(() => {
    global.sessionStorage = new StorageMock();
    global.localStorage = new StorageMock();
  });

  afterAll(() => {
    global.sessionStorage.clear();
    global.localStorage.clear();
  });

  it('Should Set Session Item', () => {
    StorageService.setItem<{ access: string }>('session_item', {
      access: 'token'
    });

    const item = global.sessionStorage.getItem('session_item');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.access).toEqual('token');
  });

  it("Should Get Session Item (As 'TEXT')", () => {
    global.sessionStorage.setItem('session_item', '{"access":"token"}');

    const item = StorageService.getItem<{ access: string }>('session_item', {
      parseJson: false
    });

    expect(item).toEqual('{"access":"token"}');
  });

  it('Should (Force) Set Local Item', () => {
    StorageService.setItem<{ access: string }>(
      'local_item',
      {
        access: 'token'
      },
      {
        forceUpdate: true,
        useLocal: true
      }
    );

    const item = global.localStorage.getItem('local_item');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.access).toEqual('token');
  });

  it("Should Get Local Item (As 'JSON')", () => {
    global.localStorage.setItem('local_item', '{"access":"token"}');
    const item = StorageService.getItem<{ access: string }>('local_item', {
      useLocal: true
    });
    expect(item.access).toEqual('token');
  });
});
