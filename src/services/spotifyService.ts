import type { Album } from '@/models/Album';
import StatusError from '@/models/StatusError';
import type { Token } from '@/models/Token';
import type { Track } from '@/models/Track';
import { generateRandomString, serialize } from '@/utils/strUtils';
import api from './api';

/**
 * Go To Spotify Connect, Then Redirect
 *
 * @param {string} clientId
 */
export const authorizeAccount = (clientId: string) => {
  const SCOPE = [
    'user-read-playback-state',
    'user-modify-playback-state',
    'user-read-currently-playing',
    'app-remote-control',
    'streaming'
  ];

  const redirectUri = import.meta.env['VITE_REDIRECT_URI'] || '';

  const params = serialize({
    response_type: 'code',
    client_id: clientId,
    scope: SCOPE.join(' '),
    redirect_uri: redirectUri,
    state: generateRandomString(16)
  });

  window.location.href = api.spotify.authorize(params);
};

/**
 * POST 'client' + 'x-www-form-urlencoded'
 *
 * @returns {Promise} { access_token }
 */
const postForm = async (client: string, form: string): Promise<Token> => {
  const response = await fetch(api.spotify.token(), {
    headers: {
      Authorization: `Basic ${client}`,
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: form,
    method: 'POST'
  });

  if (response.ok) return await response.json();
  throw new StatusError(response.statusText, response.status);
};

/**
 * POST 'clientId' + 'secretId'
 *
 * @returns {Promise} { access_token }
 */
export const grantClientCredentials = (client: string): Promise<Token> => {
  const form = serialize({
    grant_type: 'client_credentials'
  });

  return postForm(client, form);
};

/**
 * POST 'clientId' + 'secretId' (+ 'code')
 *
 * @returns {Token} { refresh_access_token }
 */
export const grantAuthorizationCode = (code: string, client: string): Promise<Token> => {
  const redirectUri = import.meta.env['VITE_REDIRECT_URI'] || '';

  const form = serialize({
    code,
    redirect_uri: redirectUri,
    grant_type: 'authorization_code'
  });

  return postForm(client, form);
};

/**
 * POST 'clientId' + 'secretId' (+ 'refreshToken')
 *
 * @returns {Promise} { access_token }
 */
export const grantRefreshToken = (token: string, client: string): Promise<Token> => {
  const form = serialize({
    refresh_token: token,
    grant_type: 'refresh_token'
  });

  return postForm(client, form);
};

/**
 * Fetch Search Query
 *
 * @param {string} query
 * @param {string} token accessToken
 * @returns {Promise}
 */
export const searchAlbums = async (query: string, token: string): Promise<{ albums: { items: Album[] } }> => {
  const response = await fetch(api.spotify.search(query), {
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    method: 'GET'
  });

  if (response.ok) return await response.json();
  throw new StatusError(response.statusText, response.status);
};

/**
 * Fetch Album By ID
 *
 * @param {string} id albumId
 * @param {string} token accessToken
 * @returns {Promise}
 */
export const getAlbum = async (id: string, token: string): Promise<Album> => {
  const response = await fetch(api.spotify.album(id), {
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    method: 'GET'
  });

  if (response.ok) return await response.json();
  throw new StatusError(response.statusText, response.status);
};

/**
 * Fetch Track By ID
 *
 * @param {string} id trackId
 * @param {string} token accessToken
 * @returns {Promise}
 */
export const getTrack = async (id: string, token: string): Promise<Track> => {
  const response = await fetch(api.spotify.track(id), {
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    method: 'GET'
  });

  if (response.ok) return await response.json();
  throw new StatusError(response.statusText, response.status);
};

/**
 * PUT Player
 *
 * @param {string} deviceId
 * @param {string} token accessToken
 * @returns {Promise}
 */
export const setDevicePlayer = async (deviceId: string, token: string): Promise<{ deviceId: string }> => {
  const response = await fetch(api.spotify.player(), {
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      device_ids: [deviceId]
    }),
    method: 'PUT'
  });

  if (response.ok) return { deviceId };
  throw new StatusError(response.statusText, response.status);
};

/**
 * POST Track (Into Queue)
 *
 * @param {string} uri trackUri
 * @param {string} token accessToken
 * @returns {Promise}
 */
export const postTrackIntoQueue = async (uri: string, token: string): Promise<{ trackUri: string }> => {
  const response = await fetch(api.spotify.queue(encodeURIComponent(uri)), {
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    method: 'POST'
  });

  if (response.ok) return { trackUri: uri };
  throw new StatusError(response.statusText, response.status);
};
