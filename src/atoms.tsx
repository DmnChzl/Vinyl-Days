import { atom } from 'jotai';
import { VIEW_TYPE } from './constants';
import type { LikedTrack } from './models/LikedTrack';
import type { Notification } from './models/Notification';
import type { SearchedAlbum } from './models/SearchedAlbum';

export const viewModeAtom = atom<string>(VIEW_TYPE.LIST);

export const isGridViewAtom = atom<boolean>(get => get(viewModeAtom) === VIEW_TYPE.GRID);

export const isListViewAtom = atom<boolean>(get => get(viewModeAtom) === VIEW_TYPE.LIST);

export const clientIdAtom = atom<string>('');
export const clientSecretAtom = atom<string>('');

export const accessTokenAtom = atom<string>('');
export const accessTokenExpirationAtom = atom<number>(0);
export const refreshTokenAtom = atom<string>('');

export const searchedAlbumsAtom = atom<SearchedAlbum[]>([]);
export const isSearchedAlbumsEmptyAtom = atom<boolean>(get => get(searchedAlbumsAtom).length === 0);

export const likedTracksAtom = atom<LikedTrack[]>([]);
export const isLikedTracksEmptyAtom = atom<boolean>(get => get(searchedAlbumsAtom).length === 0);

export const notificationQueueAtom = atom<Notification[]>([]);
export const isNotificationQueueEmptyAtom = atom<boolean>(get => get(notificationQueueAtom).length === 0);

export const modalVisibilityAtom = atom<boolean>(false);

export const playerAtom = atom<Spotify.Player | undefined>(undefined);
export const playerPausedAtom = atom<boolean>(true);
export const playerPositionAtom = atom<number>(0);

export const deviceIdAtom = atom<string>('');
