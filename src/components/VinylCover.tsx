import { ReactComponent as VinylAsset } from '@/assets/vinyl.svg';
import clsx from 'clsx';

interface Props {
  className?: string;
  sqrtSize?: number;
  imgSrc: string;
  imgAlt: string;
}

export default function VinylCover({ className, sqrtSize = 48, imgSrc, imgAlt }: Props) {
  return (
    <div
      className={clsx('relative flex flex-shrink-0', className)}
      style={{ height: sqrtSize, width: (sqrtSize / 2) * 3 }}>
      <img className="z-10 rounded-l-lg" src={imgSrc} alt={imgAlt} style={{ height: sqrtSize, width: sqrtSize }} />
      <VinylAsset className="absolute right-0 top-0 z-5" style={{ height: sqrtSize, width: sqrtSize }} />
    </div>
  );
}
