import clsx from 'clsx';
import { ElementType, ReactNode } from 'react';

interface Props {
  as?: ElementType;
  onClick?: () => void;
  onDoubleClick?: () => void;
  elementLeft?: ReactNode;
  className?: string;
  title: string;
  subTitle?: string;
  endingLabel?: string;
  elementRight?: ReactNode;
}

export default function ListItem({
  as: Component = 'div',
  elementLeft,
  className,
  title,
  subTitle,
  endingLabel,
  elementRight,
  ...props
}: Props) {
  return (
    <div className="flex w-full select-none items-center space-x-4 px-4 text-left">
      {elementLeft}

      <Component
        className={clsx('cursor-music-note flex flex-grow items-center space-x-4 border-b py-4 text-left', className)}
        {...props}>
        <div className="flex flex-grow flex-col">
          <strong className="text-neutral-900">{title}</strong>
          {subTitle && <span className="text-sm text-neutral-500">{subTitle}</span>}
        </div>

        {endingLabel && <span className="text-sm text-neutral-500">{endingLabel}</span>}
      </Component>

      {elementRight}
    </div>
  );
}
