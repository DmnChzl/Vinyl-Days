import { ReactNode } from 'react';

interface Props {
  buttonGroup: {
    onClick: () => void;
    label: string;
    disabled?: boolean;
    element?: ReactNode;
  }[];
}

export default function TrackButtonGroup({ buttonGroup = [] }: Props) {
  return (
    <div className="mx-auto flex space-x-4">
      {buttonGroup.map(({ label, disabled = false, ...props }, idx) => (
        <button
          key={idx}
          className="track-button"
          type="button"
          onClick={props.onClick}
          aria-label={label}
          disabled={disabled || false}>
          {props.element || label}
        </button>
      ))}
    </div>
  );
}
