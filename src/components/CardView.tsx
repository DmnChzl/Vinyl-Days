import clsx from 'clsx';
import { ElementType, ReactNode } from 'react';
import VinylCover from './VinylCover';

interface Props {
  as?: ElementType;
  to?: string;
  onClick?: () => void;
  fullWidth: boolean;
  coverUrl?: string;
  title: string;
  subTitle?: string;
  element?: ReactNode;
}

export default function CardView({
  as: Component = 'div',
  fullWidth = false,
  coverUrl,
  title,
  subTitle,
  element,
  ...props
}: Props) {
  return (
    <Component
      className={clsx(
        'relative flex rounded-lg bg-white p-4 shadow-sm hover:shadow',
        fullWidth ? 'space-x-4' : 'flex-col'
      )}
      {...props}>
      {coverUrl && (
        <VinylCover
          className={!fullWidth ? 'mx-auto mb-4' : ''}
          sqrtSize={64}
          imgSrc={coverUrl}
          imgAlt={`${title} Cover`}
        />
      )}

      <div className={clsx('flex flex-col justify-center', !fullWidth && 'text-center')}>
        <strong className="text-neutral-900">{title}</strong>
        {subTitle && <span className="text-sm text-neutral-500">{subTitle}</span>}
      </div>

      {element}
    </Component>
  );
}
