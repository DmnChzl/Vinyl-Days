import { ChangeEvent, ReactNode, forwardRef } from 'react';

interface Props {
  elementLeft?: ReactNode;
  type?: string;
  placeholder?: string;
  defaultValue?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  elementRight?: ReactNode;
}

const TextField = forwardRef<HTMLInputElement, Props>(({ elementLeft, elementRight, type = 'text', ...props }, ref) => (
  <div className="text-field">
    {elementLeft}
    <input
      ref={ref}
      className="flex-grow bg-transparent py-2 outline-none placeholder:text-neutral-300"
      type={type}
      {...props}
    />
    {elementRight}
  </div>
));

TextField.displayName = 'TextField';

export default TextField;
