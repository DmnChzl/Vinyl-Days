import { render, screen } from '@testing-library/react';
import VinylCover from '../VinylCover';

describe('<VinylCover />', () => {
  it('Should Renders', () => {
    render(<VinylCover sqrtSize={64} imgSrc="https://i.scdn.co/image" imgAlt="Alt" />);

    expect(screen.getByRole('img')).toHaveStyle({
      width: '64px',
      height: '64px'
    });
  });
});
