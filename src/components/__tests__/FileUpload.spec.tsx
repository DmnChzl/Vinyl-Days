import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import { vi } from 'vitest';
import FileUpload from '../FileUpload';

describe('<FileUpload />', () => {
  afterEach(cleanup);

  it('Should Renders', () => {
    render(
      <FileUpload id="file-upload" name="file-upload" label="File Upload">
        <span>Upload</span>
      </FileUpload>
    );

    expect(screen.getByText('Upload')).toBeInTheDocument();
  });

  it('Should Triggers Change Event', () => {
    const onChangeMock = vi.fn();

    render(
      <FileUpload id="file-upload" name="file-upload" onChange={onChangeMock}>
        <span>Upload</span>
      </FileUpload>
    );

    const jsonFile = new File(['{"hello":"world"}'], 'hello_world.json', {
      type: 'application/json'
    });

    fireEvent.change(screen.getByLabelText('Upload'), {
      target: { files: [jsonFile] }
    });

    expect(onChangeMock).toHaveBeenCalled();
  });
});
