import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import { vi } from 'vitest';
import ListItem from '../ListItem';

describe('<ListItem />', () => {
  afterEach(cleanup);

  it('Should Renders', () => {
    render(<ListItem title="Title" subTitle="SubTitle" endingLabel="Ending Label" />);

    expect(screen.getByText('Title')).toBeInTheDocument();
    expect(screen.getByText('SubTitle')).toBeInTheDocument();
    expect(screen.getByText('Ending Label')).toBeInTheDocument();
  });

  it('Should Renders With Elements Props', () => {
    render(
      <ListItem title="Title" elementLeft={<span>Element Left</span>} elementRight={<span>Element Right</span>} />
    );

    expect(screen.getByText('Element Left')).toBeInTheDocument();
    expect(screen.getByText('Element Right')).toBeInTheDocument();
  });

  it('Should Triggers Double Click Event', () => {
    const onDoubleClickMock = vi.fn();

    render(<ListItem as="button" onDoubleClick={onDoubleClickMock} title="Title" />);

    fireEvent.doubleClick(screen.getByRole('button'));
    expect(onDoubleClickMock).toHaveBeenCalled();
  });
});
