import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import { vi } from 'vitest';
import TextField from '../TextField';

describe('<TextField />', () => {
  afterEach(cleanup);

  it('Should Renders', () => {
    render(<TextField placeholder="Lorem Ipsum" />);
    expect(screen.getByPlaceholderText('Lorem Ipsum')).toBeInTheDocument();
  });

  it('Should Renders With Elements Props', () => {
    render(<TextField elementLeft={<span>Element Left</span>} elementRight={<span>Element Right</span>} />);

    expect(screen.getByText('Element Left')).toBeInTheDocument();
    expect(screen.getByText('Element Right')).toBeInTheDocument();
  });

  it('Should Triggers Change Event', () => {
    const onChangeMock = vi.fn();

    render(<TextField onChange={onChangeMock} />);

    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: 'Dolor Sit Amet' }
    });

    expect(onChangeMock).toHaveBeenCalled();
  });
});
