import { render, screen } from '@testing-library/react';
import Layout from '../Layout';

describe('<Layout />', () => {
  it('Should Renders', () => {
    const { unmount } = render(
      <Layout>
        <span>Lorem Ipsum Dolor Sit Amet</span>
      </Layout>
    );

    expect(screen.getByText('Lorem Ipsum Dolor Sit Amet')).toBeInTheDocument();
    unmount();
  });
});
