import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import { vi } from 'vitest';
import CardView from '../CardView';

describe('<CardView />', () => {
  afterEach(cleanup);

  it('Should Renders', () => {
    render(<CardView title="Title" subTitle="SubTitle" />);

    expect(screen.getByText('Title')).toBeInTheDocument();
    expect(screen.getByText('SubTitle')).toBeInTheDocument();
  });

  it('Should Renders With Element Prop', () => {
    render(<CardView title="Title" element={<span>Element</span>} />);
    expect(screen.getByText('Element')).toBeInTheDocument();
  });

  it('Should Triggers Click Event', () => {
    const onClickMock = vi.fn();

    render(<CardView as="button" onClick={onClickMock} fullWidth title="Title" coverUrl="https://i.scdn.co/image" />);

    fireEvent.click(screen.getByRole('button'));
    expect(onClickMock).toHaveBeenCalled();
  });
});
