import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import { vi } from 'vitest';
import TrackButtonGroup from '../TrackButtonGroup';

describe('<TrackButtonGroup />', () => {
  afterEach(cleanup);

  it('Should Renders', () => {
    const buttonGroup = [
      {
        onClick: vi.fn(),
        label: 'Default Button'
      },
      {
        onClick: vi.fn(),
        label: 'Another Button',
        element: <span>With Element</span>
      }
    ];

    render(<TrackButtonGroup buttonGroup={buttonGroup} />);

    expect(screen.getByLabelText('Default Button')).toBeInTheDocument();
    expect(screen.getByLabelText('Another Button')).toBeInTheDocument();
    expect(screen.getByText('With Element')).toBeInTheDocument();
  });

  it('Should Triggers Click Event', () => {
    const onClickMock = vi.fn();

    render(
      <TrackButtonGroup
        buttonGroup={[
          {
            onClick: onClickMock,
            label: 'Default Button'
          }
        ]}
      />
    );

    fireEvent.click(screen.getByRole('button', { name: 'Default Button' }));
    expect(onClickMock).toHaveBeenCalled();
  });
});
