import { render, screen } from '@testing-library/react';
import TimeTracker from '../TimeTracker';

describe('<TimeTracker />', () => {
  it('Should Renders', () => {
    render(<TimeTracker currentTime={30000} endTime={60000} />);

    expect(screen.getByText('0:30')).toBeInTheDocument();
    expect(screen.getByText('1:00')).toBeInTheDocument();
    expect(screen.getByRole('progressbar')).toHaveStyle({ width: '50%' });
  });
});
