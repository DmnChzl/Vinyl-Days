import { ReactComponent as DotsAsset } from '@/assets/dots.svg';
import clsx from 'clsx';
import { CSSProperties, ReactNode } from 'react';

interface Props {
  className?: string;
  children: ReactNode;
  style?: CSSProperties;
}

export default function Layout({ className, children }: Props) {
  return (
    <div className="relative h-screen">
      <DotsAsset className="fixed right-0 top-0 text-neutral-200" />
      <div className={clsx('flex h-screen flex-col', className)}>{children}</div>
      <DotsAsset className="fixed bottom-0 left-0 rotate-180 text-neutral-200" />
    </div>
  );
}
