import clsx from 'clsx';
import { ChangeEvent, ReactNode } from 'react';

interface Props {
  className?: string;
  id: string;
  name?: string;
  onChange: (file: File | undefined) => void;
  label?: string;
  children: ReactNode;
}

export default function FileUpload({ className, id, name, onChange, label, children }: Props) {
  const onUpload = (event: ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(event.target.files as FileList);

    // Check FileList > 0
    if (files.length > 0) {
      const [file] = files;

      if (typeof onChange === 'function') {
        onChange(file);
        return;
      }
    }

    if (typeof onChange === 'function') {
      onChange(undefined);
      return;
    }
  };

  return (
    <div className="relative">
      <input
        className="absolute -z-5 h-0 w-0 overflow-hidden opacity-0"
        id={id}
        name={name || id}
        type="file"
        onChange={onUpload}
      />
      <label className={clsx('cursor-pointer', className)} htmlFor={id} aria-label={label || 'File Upload'}>
        {children}
      </label>
    </div>
  );
}
