import { millisToTime } from '@/utils/dateUtils';

interface Props {
  currentTime: number;
  endTime: number;
}

export default function TimeTracker({ currentTime, endTime }: Props) {
  return (
    <div className="mx-auto flex items-center space-x-4">
      <span className="text-sm text-white">{millisToTime(currentTime)}</span>

      <div className="h-2 w-64 overflow-x-hidden rounded-full bg-blue-500">
        <div
          className="h-2 rounded-full bg-white"
          role="progressbar"
          style={{
            width: `${Math.ceil((currentTime * 100) / endTime)}%`
          }}
        />
      </div>

      <span className="text-sm text-white">{millisToTime(endTime)}</span>
    </div>
  );
}
