import { ChevronRightIcon } from '@/assets/icons';
import { DEMO } from '@/constants';
import useWindowSize from '@/hooks/useWindowSize';
import clsx from 'clsx';
import { useRef } from 'react';

interface Props {
  searchValue: string;
  handleClick: (value: string) => void;
}

export default function DemoView({ searchValue, handleClick }: Props) {
  const listRef = useRef<HTMLUListElement | null>(null);
  const windowSize = useWindowSize();

  const isChevronRightDisplayed =
    listRef.current?.scrollWidth && windowSize.width && listRef.current?.scrollWidth > windowSize.width;

  return (
    <>
      <div className="z-5 mx-4 flex items-center justify-between">
        <h3 className="text-lg text-neutral-900">
          Demo <span className="font-bold">Search</span>
        </h3>

        {isChevronRightDisplayed && <ChevronRightIcon className="m-2 text-neutral-700" aria-hidden />}
      </div>

      <div className="scrollbar-none z-5 flex h-12 flex-shrink-0 items-center overflow-x-auto">
        <ul ref={listRef} className="flex space-x-4">
          {DEMO.map((value, idx, allValues) => {
            const isEqualsToSearchValue = searchValue.toLowerCase() === value.toLowerCase();
            const isFirst = idx === 0;
            const isLast = idx === allValues.length - 1;

            return (
              <li key={idx}>
                <button
                  className={clsx(
                    'inline-flex whitespace-nowrap rounded-full px-4 py-2 shadow-sm hover:bg-blue-600 hover:text-white hover:shadow',
                    isEqualsToSearchValue ? 'bg-blue-600 text-white' : 'bg-white text-neutral-900',
                    isFirst && 'ml-4', // first:ml-4
                    isLast && 'mr-4' // last:mr-4
                  )}
                  type="button"
                  onClick={() => handleClick(value)}>
                  {value}
                </button>
              </li>
            );
          })}
        </ul>
      </div>
    </>
  );
}
