import { GridViewIcon, ListViewIcon, MoreIcon } from '@/assets/icons';
import CardView from '@/components/CardView';
import useSearchedAlbumsAtom from '@/hooks/useSearchedAlbumsAtom';
import useViewModeAtom from '@/hooks/useViewModeAtom';
import type { Album } from '@/models/Album';
import clsx from 'clsx';
import { Link } from 'react-router-dom';

interface Props {
  albums: Album[];
}

export default function ItemsView({ albums }: Props) {
  const [{ isGridView, isListView }, { toggleViewMode }] = useViewModeAtom();
  const [, { addSearchedAlbum }] = useSearchedAlbumsAtom();

  return (
    <>
      <div className="z-5 mx-4 flex items-center justify-between">
        <h3 className="text-lg text-neutral-900">
          Vinyl <span className="font-bold">{isGridView ? 'Grid' : 'List'}</span>
        </h3>

        <button className="rounded-button" type="button" onClick={toggleViewMode} aria-label="Toggle View Mode">
          {isGridView && <ListViewIcon aria-hidden />}
          {isListView && <GridViewIcon aria-hidden />}
        </button>
      </div>

      <div className="scrollbar-none z-5 mx-4 min-h-[172px] overflow-y-auto pb-4">
        <ul
          className={clsx(
            isGridView
              ? 'grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4'
              : 'flex flex-col space-y-4'
          )}>
          {albums.map((album, idx) => {
            // Extract Artist + Cover Image
            const [artist] = album.artists;
            const [cover] = album.images;

            return (
              <li key={idx}>
                <CardView
                  as={Link}
                  to={`/album/${album.id}`}
                  onClick={() => {
                    addSearchedAlbum({
                      id: album.id,
                      name: album.name
                    });
                  }}
                  fullWidth={isListView}
                  coverUrl={cover.url}
                  title={album.name}
                  subTitle={artist.name}
                  element={<MoreIcon className="absolute right-4 top-4 text-neutral-500" aria-hidden />}
                />
              </li>
            );
          })}
        </ul>
      </div>
    </>
  );
}
