import { CloseIcon, SearchIcon, UserIcon } from '@/assets/icons';
import { ReactComponent as VinylDaysAsset } from '@/assets/vinyl-days.svg';
import Layout from '@/components/Layout';
import TextField from '@/components/TextField';
import { NOTIFICATION_THEME } from '@/constants';
import { ACCOUNT_CLIENT_INFO_MSG, FETCH_SEARCH_ERROR_MSG, FETCH_TOKEN_ERROR_MSG } from '@/constants/messages';
import useClientAtom from '@/hooks/useClientAtom';
import useField from '@/hooks/useField';
import useNotificationQueueAtom from '@/hooks/useNotificationQueueAtom';
import useTokenAtom from '@/hooks/useTokenAtom';
import type { Album } from '@/models/Album';
import StatusError from '@/models/StatusError';
import * as SpotifyService from '@/services/spotifyService';
import { ChangeEvent, useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import DemoView from './DemoView';
import ItemsView from './ItemsView';

export default function Home() {
  const [{ clientId, clientSecret }] = useClientAtom();
  const [{ accessToken, refreshToken }, { setAccessToken, setAccessTokenExpiration }] = useTokenAtom();
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [searchField, setSearchField, resetSearchField] = useField('');
  const [debouncedSearch, setDebouncedSearch] = useState('');
  const [albumsItems, setAlbumsItems] = useState<Album[]>([]);
  const [, { addNotification }] = useNotificationQueueAtom();

  /**
   * Debounce Search Field Value
   */
  useEffect(() => {
    const timer = setTimeout(() => {
      setDebouncedSearch(searchField);
    }, 500);

    return () => clearTimeout(timer);
  }, [searchField, setDebouncedSearch]);

  /**
   * Fetch Token,
   * Or Throw Error (With Notification)
   *
   * @returns {string} newAccessToken
   */
  const fetchToken = async (): Promise<string> => {
    // Encoding Client ID + Secret
    const client = btoa(`${clientId}:${clientSecret}`);

    const grantService = refreshToken
      ? (c: string) => SpotifyService.grantRefreshToken(refreshToken, c)
      : SpotifyService.grantClientCredentials;

    try {
      const { access_token, expires_in } = await grantService(client);
      setAccessToken(access_token);
      setAccessTokenExpiration(Date.now() + expires_in * 1000);
      return access_token;
    } catch (err) {
      if (err instanceof StatusError && err.statusCode === 400) {
        addNotification({
          title: 'Error',
          subTitle: FETCH_TOKEN_ERROR_MSG,
          theme: NOTIFICATION_THEME.RED
        });
      }

      return '';
    }
  };

  /**
   * Fetch Search Query,
   * Then Update Items,
   * Or Throw Error (With Notification)
   *
   * @param {string} searchQuery
   */
  const fetchSearch = async (searchQuery: string, currentAccessToken: string): Promise<void> => {
    // ? No Token Yet ?
    if (!currentAccessToken) {
      currentAccessToken = await fetchToken();
    }

    try {
      const { albums } = await SpotifyService.searchAlbums(searchQuery, currentAccessToken);
      setAlbumsItems(albums.items);
    } catch (err) {
      // ? Token Expired ?
      if (err instanceof StatusError && err.statusCode === 401) {
        const newAccessToken = await fetchToken();
        return fetchSearch(searchQuery, newAccessToken);
      }

      addNotification({
        title: 'Error',
        subTitle: FETCH_SEARCH_ERROR_MSG,
        theme: NOTIFICATION_THEME.RED
      });
    }
  };

  useEffect(() => {
    if (debouncedSearch.length >= 3) {
      if (clientId && clientSecret) fetchSearch(debouncedSearch, accessToken);

      if (!clientId || !clientSecret) {
        addNotification({
          title: 'Info',
          subTitle: ACCOUNT_CLIENT_INFO_MSG,
          theme: NOTIFICATION_THEME.RED
        });
      }
    }
  }, [clientId, clientSecret, debouncedSearch, accessToken]);

  const presetSearch = (value: string) => {
    if (inputRef.current) {
      inputRef.current.value = value;
      setSearchField({
        target: { value }
      } as ChangeEvent<HTMLInputElement>);
    }
  };

  const resetSearch = () => {
    if (inputRef.current) {
      inputRef.current.value = '';
      resetSearchField();

      // Reset List / Grid
      setAlbumsItems([]);
    }
  };

  return (
    <Layout className="space-y-4 pt-4">
      <div className="z-5 mx-4 flex justify-between">
        <div className="flex items-center space-x-4">
          <VinylDaysAsset className="text-neutral-900" width={64} height={64} />

          <div className="flex flex-col text-lg text-neutral-900">
            <h1>
              <span style={{ fontWeight: 700 }}>VINYL</span> DAYS
            </h1>
            <h2>
              Explore <span style={{ fontWeight: 700 }}>Music</span>
            </h2>
          </div>
        </div>

        <Link to="/account" className="rounded-button self-start" aria-label="Go To Account">
          <UserIcon aria-hidden />
        </Link>
      </div>

      <TextField
        elementLeft={<SearchIcon className="text-neutral-500" aria-hidden />}
        ref={inputRef}
        placeholder="Dial M For Monkey"
        defaultValue={searchField}
        onChange={setSearchField}
        elementRight={
          searchField && (
            <button
              className="text-neutral-500 hover:text-neutral-900"
              type="button"
              onClick={resetSearch}
              aria-label="Reset Search Field">
              <CloseIcon aria-hidden />
            </button>
          )
        }
      />

      <DemoView searchValue={searchField} handleClick={presetSearch} />
      {albumsItems.length > 0 && <ItemsView albums={albumsItems} />}
    </Layout>
  );
}
