import ModalPortal from '@/ModalProvider';
import { DataObjectIcon, DeleteIcon, DownloadIcon, UploadIcon } from '@/assets/icons';
import { modalVisibilityAtom } from '@/atoms';
import FileUpload from '@/components/FileUpload';
import { APP_STATE_KEY, NOTIFICATION_THEME } from '@/constants';
import { APPLICATION_DATA_INFO_MSG, CORRUPTED_FILE_ERROR_MSG } from '@/constants/messages';
import useNotificationQueueAtom from '@/hooks/useNotificationQueueAtom';
import { AppState, instanceOfAppState } from '@/models/AppState';
import * as StorageService from '@/services/storageService';
import { downloadFile, readFileAsText } from '@/utils/fileUtils';
import { useSetAtom } from 'jotai';
import { useNavigate } from 'react-router-dom';

export default function DataManagementModal() {
  const navigate = useNavigate();
  const setModalVisibility = useSetAtom(modalVisibilityAtom);
  const [, { addNotification }] = useNotificationQueueAtom();

  /**
   * Get Local Storage Item,
   * Then Download It As JSON
   */
  const download = () => {
    const appState = StorageService.getItem<AppState>(APP_STATE_KEY, {
      useLocal: true
    }) as Partial<AppState>;

    const jsonFile = new File([JSON.stringify(appState)], APP_STATE_KEY + '.json', {
      type: 'application/json'
    });

    downloadFile(jsonFile);
    setModalVisibility(false);
  };

  /**
   * Read File As Text,
   * Then Set Local Storage Item
   *
   * @param file
   */
  const upload = async (file: File | undefined) => {
    // Check File !== undefined
    if (file) {
      const text = await readFileAsText(file);
      const json = JSON.parse(text);

      if (instanceOfAppState(json)) {
        StorageService.setItem<AppState>(APP_STATE_KEY, json, {
          forceUpdate: true,
          useLocal: true
        });

        navigate(0); // Refresh Page
      } else {
        addNotification({
          title: 'Error',
          subTitle: CORRUPTED_FILE_ERROR_MSG,
          theme: NOTIFICATION_THEME.RED
        });
      }
    }

    setModalVisibility(false);
  };

  const resetData = () => {
    const { createdAt, modifiedAt, clientId, clientSecret } = StorageService.getItem<AppState>(APP_STATE_KEY, {
      useLocal: true
    }) as Partial<AppState>;

    StorageService.setItem<AppState>(
      APP_STATE_KEY,
      { createdAt, modifiedAt, clientId, clientSecret },
      {
        forceUpdate: true,
        useLocal: true
      }
    );

    navigate(0); // Refresh Page
  };

  return (
    <ModalPortal>
      <span className="mx-auto flex h-12 w-12 rounded-full bg-blue-100 text-blue-600">
        <DataObjectIcon className="m-auto" width={36} height={36} aria-hidden />
      </span>

      <div className="mx-auto flex flex-col text-center">
        <span className="text-lg font-bold text-neutral-900">Data Management</span>
        <span className="text-neutral-500">{APPLICATION_DATA_INFO_MSG}</span>
      </div>

      <div className="mx-auto flex justify-center space-x-4">
        <button
          className="flex w-full space-x-2 rounded-full border border-neutral-900 py-2 pl-3 pr-4 text-neutral-900 hover:border-blue-600 hover:text-blue-600"
          type="button"
          onClick={download}
          aria-label="Download">
          <DownloadIcon aria-hidden />
          <span>Download</span>
        </button>

        <FileUpload
          className="flex w-full space-x-2 rounded-full border border-neutral-900 py-2 pl-3 pr-4 text-neutral-900 hover:border-blue-600 hover:text-blue-600"
          id="file-upload"
          onChange={upload}
          label="Upload">
          <UploadIcon aria-hidden />
          <span>Upload</span>
        </FileUpload>

        <button
          className="flex w-full space-x-2 rounded-full border border-red-600 bg-white py-2 pl-3 pr-4 text-red-600 hover:border-white hover:bg-red-600 hover:text-white"
          type="button"
          onClick={resetData}
          aria-label="Reset Data">
          <DeleteIcon aria-hidden />
          <span>Reset</span>
        </button>
      </div>
    </ModalPortal>
  );
}
