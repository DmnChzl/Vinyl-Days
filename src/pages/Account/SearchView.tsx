import { ChevronRightIcon } from '@/assets/icons';
import useSearchedAlbumsAtom from '@/hooks/useSearchedAlbumsAtom';
import useWindowSize from '@/hooks/useWindowSize';
import { sortBy } from '@/utils/arrUtils';
import clsx from 'clsx';
import { useRef } from 'react';
import { Link } from 'react-router-dom';

export default function SearchView() {
  const listRef = useRef<HTMLUListElement | null>(null);
  const windowSize = useWindowSize();
  const [{ searchedAlbums }] = useSearchedAlbumsAtom();

  if (searchedAlbums.length === 0) return null;

  const isChevronRightDisplayed =
    listRef.current?.scrollWidth && windowSize.width && listRef.current?.scrollWidth > windowSize.width;

  return (
    <>
      <div className="z-5 mx-4 flex items-center justify-between">
        <h3 className="text-lg text-neutral-900">
          Last <span className="font-bold">Search</span>
        </h3>

        {isChevronRightDisplayed && <ChevronRightIcon className="m-2 text-neutral-700" aria-hidden />}
      </div>

      <div className="scrollbar-none z-5 flex h-12 flex-shrink-0 items-center overflow-x-auto">
        <ul ref={listRef} className="flex space-x-4">
          {searchedAlbums.sort(sortBy('timestamp', 'desc')).map((album, idx, allAlbums) => {
            const isFirst = idx === 0;
            const isLast = idx === allAlbums.length - 1;

            return (
              <li key={idx}>
                <Link
                  to={`/album/${album.id}`}
                  className={clsx(
                    'inline-flex whitespace-nowrap rounded-full bg-white px-4 py-2 text-neutral-900 shadow-sm hover:bg-blue-600 hover:text-white hover:shadow',
                    isFirst && 'ml-4', // first:ml-4
                    isLast && 'mr-4' // last:mr-4
                  )}>
                  {album.name}
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    </>
  );
}
