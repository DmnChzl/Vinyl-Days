import { ArrowBackIcon, DataObjectIcon, KeyIcon, VisibilityIcon, VisibilityOffIcon, VpnKeyIcon } from '@/assets/icons';
import { ReactComponent as SpotifyAsset } from '@/assets/spotify.svg';
import { modalVisibilityAtom } from '@/atoms';
import Layout from '@/components/Layout';
import TextField from '@/components/TextField';
import useClientAtom from '@/hooks/useClientAtom';
import * as SpotifyService from '@/services/spotifyService';
import { useSetAtom } from 'jotai';
import { ChangeEvent, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import DataManagementModal from './DataManagementModal';
import FavoriteView from './FavoriteView';
import SearchView from './SearchView';

export default function Account() {
  const navigate = useNavigate();
  const [{ clientId, clientSecret }, { setClientId, setClientSecret }] = useClientAtom();
  const setModalVisibility = useSetAtom(modalVisibilityAtom);

  const [inputIdType, setInputIdType] = useState<string>('password');
  const toggleInputIdType = () => setInputIdType(val => (val === 'text' ? 'password' : 'text'));

  const [inputSecretType, setInputSecretType] = useState<string>('password');
  const toggleInputSecretType = () => setInputSecretType(val => (val === 'text' ? 'password' : 'text'));

  return (
    <Layout className="space-y-4 pt-4">
      <DataManagementModal />

      <div className="z-5 mx-4 flex items-center space-x-4">
        <button className="rounded-button" type="button" onClick={() => navigate(-1)} aria-label="Go Back">
          <ArrowBackIcon aria-hidden />
        </button>

        <h2 className="flex-grow text-lg text-neutral-900">
          My <span className="font-bold">Account</span>
        </h2>

        <button
          className="rounded-button self-start"
          type="button"
          onClick={() => setModalVisibility(true)}
          aria-label="Download Archive">
          <DataObjectIcon aria-hidden />
        </button>
      </div>

      <TextField
        elementLeft={<KeyIcon className="text-neutral-500" aria-hidden />}
        type={inputIdType}
        placeholder="Client ID"
        defaultValue={clientId}
        onChange={(e: ChangeEvent<HTMLInputElement>) => setClientId(e.target.value)}
        elementRight={
          <button
            className="text-neutral-500 hover:text-neutral-900"
            type="button"
            onClick={toggleInputIdType}
            aria-label="Toggle Input Type">
            {inputIdType === 'text' ? <VisibilityIcon aria-hidden /> : <VisibilityOffIcon aria-hidden />}
          </button>
        }
      />

      <TextField
        elementLeft={<VpnKeyIcon className="text-neutral-500" aria-hidden />}
        type={inputSecretType}
        placeholder="Client Secret"
        defaultValue={clientSecret}
        onChange={(e: ChangeEvent<HTMLInputElement>) => setClientSecret(e.target.value)}
        elementRight={
          <button
            className="text-neutral-500 hover:text-neutral-900"
            type="button"
            onClick={toggleInputSecretType}
            aria-label="Toggle Input Type">
            {inputSecretType === 'text' ? <VisibilityIcon aria-hidden /> : <VisibilityOffIcon aria-hidden />}
          </button>
        }
      />

      <button
        className="z-5 mx-4 flex justify-center space-x-2 rounded-full bg-neutral-900 px-4 py-2 text-white shadow-sm hover:bg-white hover:text-neutral-900 hover:shadow disabled:bg-neutral-500 disabled:text-neutral-100"
        type="button"
        onClick={() => SpotifyService.authorizeAccount(clientId)}
        disabled={!clientId || !clientSecret}>
        <SpotifyAsset aria-hidden />
        <span className="font-bold">Spotify Connect</span>
      </button>

      <SearchView />
      <FavoriteView />
    </Layout>
  );
}
