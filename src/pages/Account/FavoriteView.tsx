import { GridViewIcon, HeartFilledIcon, ListViewIcon } from '@/assets/icons';
import CardView from '@/components/CardView';
import useLikedTracksAtom from '@/hooks/useLikedTracksAtom';
import useViewModeAtom from '@/hooks/useViewModeAtom';
import { sortBy } from '@/utils/arrUtils';
import clsx from 'clsx';

export default function FavoriteView() {
  const [{ isGridView, isListView }, { toggleViewMode }] = useViewModeAtom();
  const [{ likedTracks }, { delLikedTrack }] = useLikedTracksAtom();

  if (likedTracks.length === 0) return null;

  return (
    <>
      <div className="z-5 mx-4 flex items-center justify-between">
        <h3 className="text-lg text-neutral-900">
          Favorite <span className="font-bold">{isGridView ? 'Grid' : 'List'}</span>
        </h3>

        <button
          className="rounded-button self-start"
          type="button"
          onClick={toggleViewMode}
          aria-label="Toggle View Mode">
          {isGridView && <ListViewIcon aria-hidden />}
          {isListView && <GridViewIcon aria-hidden />}
        </button>
      </div>

      <div className="scrollbar-none z-5 mx-4 min-h-[172px] overflow-y-auto pb-4">
        <ul
          className={clsx(
            isGridView
              ? 'grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4'
              : 'flex flex-col space-y-4'
          )}>
          {likedTracks.sort(sortBy('timestamp', 'desc')).map((track, idx) => (
            <li key={idx}>
              <CardView
                fullWidth={isListView}
                coverUrl={track.coverUrl}
                title={track.name}
                subTitle={track.artist}
                element={
                  <button
                    className="absolute right-2 top-2 rounded-full p-2 hover:bg-neutral-100"
                    type="button"
                    onClick={() => delLikedTrack(track.id)}
                    aria-label="Like Track">
                    <HeartFilledIcon className="text-red-600 hover:text-neutral-700" aria-hidden />
                  </button>
                }
              />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}
