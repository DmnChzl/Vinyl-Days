import { ReactComponent as VinylAsset } from '@/assets/vinyl.svg';
import { PLAY_STATE } from '@/constants';
import usePlayerAtom from '@/hooks/usePlayerAtom';

interface Props {
  coverUrl: string;
  albumName: string;
}

export default function VinylAnimation({ coverUrl, albumName }: Props) {
  const { isPlaying } = usePlayerAtom();
  return (
    <div className="absolute-center z-10 flex rounded-full bg-neutral-100 p-4" style={{ top: 0 }}>
      <div className="relative flex h-64 w-64">
        <img
          className="z-15 m-auto h-24 w-24 animate-spin-slow rounded-full"
          src={coverUrl}
          alt={`${albumName} Cover`}
          style={{
            animationPlayState: isPlaying ? PLAY_STATE.RUNNING : PLAY_STATE.PAUSED
          }}
        />

        <VinylAsset className="absolute inset-0 z-10 h-64 w-64" />
      </div>
    </div>
  );
}
