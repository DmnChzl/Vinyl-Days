import TimeTracker from '@/components/TimeTracker';
import usePlayerAtom from '@/hooks/usePlayerAtom';
import { useTimer } from '@/hooks/useTimer';
import { useEffect } from 'react';

interface Props {
  trackId: string;
  trackDuration: number;
  onTimeOver?: () => void;
}

export default function TimeTrackerWrapper({
  trackId,
  trackDuration,
  onTimeOver = () => {} // NOOP
}: Props) {
  const { isPlaying, position } = usePlayerAtom();

  const {
    time: currentTime,
    start: startTimer,
    pause: pauseTimer,
    set: setTime
  } = useTimer({
    endTime: trackDuration,
    intervalPeriod: 250,
    onTimeOver,
    stepperCount: 250
  });

  useEffect(() => {
    if (trackId) setTime(0);
  }, [trackId]);

  useEffect(() => {
    if (isPlaying) {
      startTimer();
    } else {
      pauseTimer();
    }
  }, [isPlaying]);

  useEffect(() => {
    setTime(position);
  }, [position]);

  return <TimeTracker currentTime={currentTime} endTime={trackDuration} />;
}
