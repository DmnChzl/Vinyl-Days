import {
  ArrowBackIcon,
  CloseIcon,
  HeartFilledIcon,
  HeartIcon,
  PauseIcon,
  PlayIcon,
  ShuffleIcon,
  SkipNextIcon,
  SkipPreviousIcon
} from '@/assets/icons';
import Layout from '@/components/Layout';
import TrackButtonGroup from '@/components/TrackButtonGroup';
import { NOTIFICATION_THEME } from '@/constants';
import {
  FETCH_TOKEN_ERROR_MSG,
  FETCH_TRACK_ERROR_MSG,
  getFavoriteLikeMsg,
  getFavoriteUnlikeMsg
} from '@/constants/messages';
import useClientAtom from '@/hooks/useClientAtom';
import useLikedTrackAtom from '@/hooks/useLikedTracksAtom';
import useNotificationQueueAtom from '@/hooks/useNotificationQueueAtom';
import usePlayerAtom from '@/hooks/usePlayerAtom';
import useToggle from '@/hooks/useToggle';
import useTokenAtom from '@/hooks/useTokenAtom';
import type { LikedTrack } from '@/models/LikedTrack';
import StatusError from '@/models/StatusError';
import type { Track } from '@/models/Track';
import * as SpotifyService from '@/services/spotifyService';
import { getAnotherRandomValue, getNextValue, getPreviousValue } from '@/utils/arrUtils';
import { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import TimeTrackerWrapper from './TimeTrackerWrapper';
import VinylAnimation from './VinylAnimation';

const DEFAULT_TRACK = {
  album: {
    artists: [],
    id: '',
    images: [],
    name: '',
    release_date: '',
    total_tracks: 0
  },
  artists: [],
  duration_ms: 0,
  id: '',
  name: '',
  track_number: 0,
  uri: ''
};

export default function NowPlaying() {
  const { trackId } = useParams();
  const navigate = useNavigate();
  const [{ clientId, clientSecret }] = useClientAtom();

  /**
   * If There Is No Client,
   * Then Redirect To <Home />
   */
  if (!clientId || !clientSecret) navigate('/');

  const [{ accessToken, refreshToken }, { setAccessToken, setAccessTokenExpiration }] = useTokenAtom();
  const [isTimeOver, timeOverOn, timeOverOff] = useToggle(); // false
  const { player, isPlaying, deviceId } = usePlayerAtom();
  const [itemTrack, setTrackItem] = useState<Track>(DEFAULT_TRACK);
  const [albumTracksId, setAlbumTracksId] = useState<string[]>([]);
  const [, { isLikedTracksIncludesId, addLikedTrack, delLikedTrack }] = useLikedTrackAtom();
  const [, { addNotification }] = useNotificationQueueAtom();

  useEffect(() => {
    /**
     * Avoid Back Effect From:
     * - Arrow Button
     * - Close Button
     * - Browser
     * - Keyboard
     */
    return () => {
      player?.pause();
    };
  }, [player]);

  /**
   * Fetch Token,
   * Or Throw Error (With Notification)
   *
   * @returns {string} newAccessToken
   */
  const fetchToken = async (): Promise<string> => {
    // Encoding Client ID + Secret
    const client = btoa(`${clientId}:${clientSecret}`);

    const grantService = refreshToken
      ? (c: string) => SpotifyService.grantRefreshToken(refreshToken, c)
      : SpotifyService.grantClientCredentials;

    try {
      const { access_token, expires_in } = await grantService(client);
      setAccessToken(access_token);
      setAccessTokenExpiration(Date.now() + expires_in * 1000);
      return access_token;
    } catch (err) {
      if (err instanceof StatusError && err.statusCode === 400) {
        addNotification({
          title: 'Error',
          subTitle: FETCH_TOKEN_ERROR_MSG,
          theme: NOTIFICATION_THEME.RED
        });
      }

      return '';
    }
  };

  /**
   * Add To Queue,
   * Then Switch To Next Track + Implicit Resume
   *
   * @param {string} uri trackUri
   * @param {string} currentAccessToken
   */
  const playTrack = async (uri: string, currentAccessToken: string) => {
    try {
      await SpotifyService.postTrackIntoQueue(uri, currentAccessToken);
      await player?.nextTrack();
      timeOverOff();
    } catch {
      timeOverOn();
    }
  };

  /**
   * Fetch Track By ID,
   * Then Update Item,
   * Or Throw Error (With Notification)
   *
   * @param {string} id
   * @param {string} currentAccessToken
   */
  const fetchTrack = async (id: string, currentAccessToken: string): Promise<void> => {
    // ? No Token Yet ?
    if (!currentAccessToken) {
      currentAccessToken = await fetchToken();
    }

    try {
      const item = await SpotifyService.getTrack(id, currentAccessToken);
      setTrackItem(item);

      playTrack(item.uri, currentAccessToken);

      // (Re)Fetch Album By ID, Then Update Items
      const { tracks } = await SpotifyService.getAlbum(item.album.id, currentAccessToken);
      setAlbumTracksId(tracks.items.map(item => item.id));
    } catch (err) {
      // ? Token Expired ?
      if (err instanceof StatusError && err.statusCode === 401) {
        const newAccessToken = await fetchToken();
        return fetchTrack(id, newAccessToken);
      }

      addNotification({
        title: 'Error',
        subTitle: FETCH_TRACK_ERROR_MSG,
        theme: NOTIFICATION_THEME.RED
      });
    }
  };

  useEffect(() => {
    if (deviceId && trackId) fetchTrack(trackId, accessToken);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [deviceId, trackId]);

  /**
   * Handle Like / Unlike
   *
   * @param {LikedTrack} track { id, name, artist, coverUrl }
   */
  const likeTrack = (track: LikedTrack) => {
    if (isLikedTracksIncludesId(track.id)) {
      delLikedTrack(track.id);
    } else {
      addLikedTrack(track);
    }
  };

  const likeTrackThenNotify = (trackId: string, trackName: string, artistName: string, coverUrl: string) => {
    likeTrack({
      id: trackId,
      name: trackName,
      artist: artistName,
      coverUrl
    });

    const isLikedTrack = isLikedTracksIncludesId(trackId);

    addNotification({
      title: isLikedTrack ? 'Unliked !' : 'Liked !',
      subTitle: isLikedTrack ? getFavoriteUnlikeMsg(trackName) : getFavoriteLikeMsg(trackName),
      theme: NOTIFICATION_THEME.LIGHT
    });
  };

  // Extract Artist + Cover Image
  const [artist] = itemTrack.album.artists;
  const [cover] = itemTrack.album.images;

  return (
    <Layout className="justify-between">
      <div className="z-5 flex items-center space-x-4 p-4">
        <Link to={`/album/${itemTrack.album.id}`} className="rounded-button" aria-label="Go Back">
          <ArrowBackIcon aria-hidden />
        </Link>

        <h2 className="flex-grow text-lg text-neutral-900">
          Now <strong>Playing</strong>
        </h2>

        <Link to="/" className="rounded-button" aria-label="Close">
          <CloseIcon aria-hidden />
        </Link>
      </div>

      <div className="-shadow-sm relative z-5 flex max-h-[calc(100vh-144px)] min-h-[50%] w-full flex-col rounded-t-[16px] bg-blue-600 text-white sm:mx-auto sm:w-[512px]">
        {cover && <VinylAnimation coverUrl={cover?.url} albumName={itemTrack.album?.name} />}

        <div className="h-36 w-full" />

        <div className="flex flex-col space-y-4 p-4">
          <div className="flex flex-col items-center">
            <strong className="text-lg">{itemTrack.name}</strong>
            <span>{itemTrack.artists.map(artist => artist.name).join(', ')}</span>
          </div>

          <TimeTrackerWrapper
            trackId={itemTrack.id}
            trackDuration={itemTrack.duration_ms}
            onTimeOver={() => {
              // ! Sometimes The Player Continue To Play...
              // ! This Is Due To 'time' ~= 'endTime', But Not 'time' >= 'endTime'
              timeOverOn();
              player?.pause();
            }}
          />

          <TrackButtonGroup
            buttonGroup={[
              {
                onClick: () => {
                  const randomId = getAnotherRandomValue<string>(albumTracksId, itemTrack.id);
                  navigate(`/playing/${randomId}`);
                },
                label: 'Random Track',
                disabled: albumTracksId.length === 1,
                element: <ShuffleIcon aria-hidden />
              },
              {
                onClick: () => {
                  const previousId = getPreviousValue<string>(albumTracksId, itemTrack.id);
                  navigate(`/playing/${previousId}`);
                },
                label: 'Previous Track',
                disabled: albumTracksId.indexOf(itemTrack.id) === 0,
                element: <SkipPreviousIcon aria-hidden />
              },
              {
                onClick: () => {
                  if (isTimeOver) {
                    playTrack(itemTrack.uri, accessToken);
                  } else {
                    player?.togglePlay();
                  }
                },
                label: 'Play / Pause',
                element: isPlaying ? <PauseIcon aria-hidden /> : <PlayIcon aria-hidden />
              },
              {
                onClick: () => {
                  const nextId = getNextValue<string>(albumTracksId, itemTrack.id);
                  navigate(`/playing/${nextId}`);
                },
                label: 'Next Track',
                disabled: albumTracksId.indexOf(itemTrack.id) === albumTracksId.length - 1,
                element: <SkipNextIcon aria-hidden />
              },
              {
                onClick: () => {
                  likeTrackThenNotify(itemTrack.id, itemTrack.name, artist.name, cover.url);
                },
                label: 'Like Track',
                element: isLikedTracksIncludesId(itemTrack.id) ? (
                  <HeartFilledIcon aria-hidden />
                ) : (
                  <HeartIcon aria-hidden />
                )
              }
            ]}
          />
        </div>
      </div>
    </Layout>
  );
}
