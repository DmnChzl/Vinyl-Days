import { ArrowBackIcon, ExpandLessIcon, ExpandMoreIcon, HeartFilledIcon, HeartIcon } from '@/assets/icons';
import Layout from '@/components/Layout';
import ListItem from '@/components/ListItem';
import VinylCover from '@/components/VinylCover';
import { NOTIFICATION_THEME } from '@/constants';
import {
  ACCOUNT_SPOTIFY_INFO_MSG,
  FETCH_ALBUM_ERROR_MSG,
  FETCH_TOKEN_ERROR_MSG,
  getFavoriteLikeMsg,
  getFavoriteUnlikeMsg
} from '@/constants/messages';
import useClientAtom from '@/hooks/useClientAtom';
import useLikedTrackAtom from '@/hooks/useLikedTracksAtom';
import useNotificationQueueAtom from '@/hooks/useNotificationQueueAtom';
import useToggle from '@/hooks/useToggle';
import useTokenAtom from '@/hooks/useTokenAtom';
import type { Album } from '@/models/Album';
import type { LikedTrack } from '@/models/LikedTrack';
import StatusError from '@/models/StatusError';
import * as SpotifyService from '@/services/spotifyService';
import { isExpired, millisToTime } from '@/utils/dateUtils';
import clsx from 'clsx';
import { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';

const DEFAULT_ALBUM = {
  artists: [],
  id: '',
  images: [],
  name: '',
  release_date: '',
  total_tracks: 0,
  tracks: {
    items: []
  }
};

export default function CurrentAlbum() {
  const { albumId } = useParams();
  const navigate = useNavigate();
  const [{ clientId, clientSecret }] = useClientAtom();

  /**
   * If There Is No Client,
   * Then Redirect To <Home />
   */
  if (!clientId || !clientSecret) navigate('/');

  const [{ accessToken, accessTokenExpiration, refreshToken }, { setAccessToken, setAccessTokenExpiration }] =
    useTokenAtom();
  const [expanded, , , toggleExpanded] = useToggle(true);
  const [albumItem, setAlbumItem] = useState<Album>(DEFAULT_ALBUM);
  const [, { isLikedTracksIncludesId, addLikedTrack, delLikedTrack }] = useLikedTrackAtom();
  const [, { addNotification }] = useNotificationQueueAtom();

  /**
   * Fetch Token,
   * Or Throw Error (With Notification)
   *
   * @returns {string} newAccessToken
   */
  const fetchToken = async (): Promise<string> => {
    // Encoding Client ID + Secret
    const client = btoa(`${clientId}:${clientSecret}`);

    const grantService = refreshToken
      ? (c: string) => SpotifyService.grantRefreshToken(refreshToken, c)
      : SpotifyService.grantClientCredentials;

    try {
      const { access_token, expires_in } = await grantService(client);
      setAccessToken(access_token);
      setAccessTokenExpiration(Date.now() + expires_in * 1000);
      return access_token;
    } catch (err) {
      if (err instanceof StatusError && err.statusCode === 400) {
        addNotification({
          title: 'Error',
          subTitle: FETCH_TOKEN_ERROR_MSG,
          theme: NOTIFICATION_THEME.RED
        });
      }

      return '';
    }
  };

  /**
   * Fetch Album By ID,
   * Then Update Item,
   * Or Throw Error (With Notification)
   *
   * @param {string} id
   * @param {string} currentAccessToken
   */
  const fetchAlbum = async (id: string, currentAccessToken: string): Promise<void> => {
    // ? No Token Yet ?
    if (!currentAccessToken) {
      currentAccessToken = await fetchToken();
    }

    try {
      const item = await SpotifyService.getAlbum(id, currentAccessToken);
      setAlbumItem(item);
    } catch (err) {
      // ? Token Expired ?
      if (err instanceof StatusError && err.statusCode === 401) {
        const newAccessToken = await fetchToken();
        return fetchAlbum(id, newAccessToken);
      }

      addNotification({
        title: 'Error',
        subTitle: FETCH_ALBUM_ERROR_MSG,
        theme: NOTIFICATION_THEME.RED
      });
    }
  };

  useEffect(() => {
    if (albumId) fetchAlbum(albumId, accessToken);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [albumId]);

  /**
   * Handle Like / Unlike
   *
   * @param {LikedTrack} track { id, name, artist, coverUrl }
   */
  const likeTrack = (track: LikedTrack) => {
    if (isLikedTracksIncludesId(track.id)) {
      delLikedTrack(track.id);
    } else {
      addLikedTrack(track);
    }
  };

  const likeTrackThenNotify = (trackId: string, trackName: string, artistName: string, coverUrl: string) => {
    likeTrack({
      id: trackId,
      name: trackName,
      artist: artistName,
      coverUrl
    });

    const isLikedTrack = isLikedTracksIncludesId(trackId);

    addNotification({
      title: isLikedTrack ? 'Unliked !' : 'Liked !',
      subTitle: isLikedTrack ? getFavoriteUnlikeMsg(trackName) : getFavoriteLikeMsg(trackName),
      theme: NOTIFICATION_THEME.BLUE
    });
  };

  const goToPlaying = (trackId: string) => {
    // ? !!refreshToken === 'Spotify Premium Account' ?
    if (refreshToken && accessTokenExpiration && !isExpired(accessTokenExpiration)) {
      navigate(`/playing/${trackId}`);
    } else {
      addNotification({
        title: 'Info',
        subTitle: ACCOUNT_SPOTIFY_INFO_MSG,
        theme: NOTIFICATION_THEME.BLUE
      });
    }
  };

  // Extract Artist + Cover Image
  const [artist] = albumItem.artists;
  const [cover] = albumItem.images;

  return (
    <Layout>
      <div className="flex flex-col space-y-4 p-4">
        <div className="z-5 flex items-center space-x-4">
          <Link to="/" className="rounded-button" aria-label="Go Back">
            <ArrowBackIcon aria-hidden />
          </Link>

          <h2 className="flex-grow text-lg text-neutral-900">
            Current <strong>Album</strong>
          </h2>

          <button className="rounded-button" type="button" onClick={toggleExpanded} aria-label="Toggle Expand">
            {expanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
          </button>
        </div>

        {cover && (
          <div className={clsx('z-5 mx-4 flex-col space-y-4', expanded ? 'flex' : 'hidden')}>
            <VinylCover className="mx-auto" sqrtSize={192} imgSrc={cover?.url} imgAlt={`${albumItem.name} Cover`} />

            <div className="flex flex-col items-center">
              <strong className="text-lg text-neutral-900">{albumItem.name}</strong>
              <span className="text-neutral-500">{artist.name}</span>
            </div>
          </div>
        )}
      </div>

      <div className="-shadow-sm scrollbar-none z-5 mt-auto min-h-[76px] w-full overflow-y-auto rounded-t-[16px] bg-white sm:mx-auto sm:w-[512px]">
        <ul className="flex flex-col">
          {albumItem.tracks.items.map((track, idx, allTracks) => {
            const isLast = idx === allTracks.length - 1;
            const allArtists = track.artists.map(artist => artist.name).join(', ');
            const trackNumber = `${track.track_number}`.padStart(2, '0');

            return (
              <li key={idx}>
                <ListItem
                  as="button"
                  onClick={() => goToPlaying(track.id)}
                  className={isLast ? 'border-white' : 'border-neutral-100'}
                  elementLeft={<span className="text-neutral-500">{trackNumber}</span>}
                  title={track.name}
                  subTitle={allArtists}
                  endingLabel={millisToTime(track.duration_ms)}
                  elementRight={
                    <button
                      className="rounded-full p-2 hover:bg-neutral-100"
                      type="button"
                      onClick={() => {
                        likeTrackThenNotify(track.id, track.name, artist.name, cover.url);
                      }}
                      aria-label="Like Track">
                      {isLikedTracksIncludesId(track.id) ? (
                        <HeartFilledIcon className="text-red-600 hover:text-neutral-700" aria-hidden />
                      ) : (
                        <HeartIcon className="text-neutral-700 hover:text-red-600" aria-hidden />
                      )}
                    </button>
                  }
                />
              </li>
            );
          })}
        </ul>
      </div>
    </Layout>
  );
}
