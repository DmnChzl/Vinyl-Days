import { ReactComponent as ArrowBackIcon } from './arrow-back.svg';
import { ReactComponent as BellActiveIcon } from './bell-active.svg';
import { ReactComponent as ChevronRightIcon } from './chevron-right.svg';
import { ReactComponent as CloseIcon } from './close.svg';
import { ReactComponent as DataObjectIcon } from './data-object.svg';
import { ReactComponent as DeleteIcon } from './delete.svg';
import { ReactComponent as DownloadIcon } from './download.svg';
import { ReactComponent as ExpandLessIcon } from './expand-less.svg';
import { ReactComponent as ExpandMoreIcon } from './expand-more.svg';
import { ReactComponent as GridViewIcon } from './grid-view.svg';
import { ReactComponent as HeartFilledIcon } from './heart-filled.svg';
import { ReactComponent as HeartIcon } from './heart.svg';
import { ReactComponent as KeyIcon } from './key.svg';
import { ReactComponent as ListViewIcon } from './list-view.svg';
import { ReactComponent as MoreIcon } from './more.svg';
import { ReactComponent as PauseIcon } from './pause.svg';
import { ReactComponent as PlayIcon } from './play.svg';
import { ReactComponent as RepeatIcon } from './repeat.svg';
import { ReactComponent as SearchIcon } from './search.svg';
import { ReactComponent as ShuffleIcon } from './shuffle.svg';
import { ReactComponent as SkipNextIcon } from './skip-next.svg';
import { ReactComponent as SkipPreviousIcon } from './skip-previous.svg';
import { ReactComponent as UploadIcon } from './upload.svg';
import { ReactComponent as UserIcon } from './user.svg';
import { ReactComponent as VisibilityOffIcon } from './visibility-off.svg';
import { ReactComponent as VisibilityIcon } from './visibility.svg';
import { ReactComponent as VpnKeyIcon } from './vpn-key.svg';

// https://fonts.google.com/icons
export {
  ArrowBackIcon,
  BellActiveIcon,
  ChevronRightIcon,
  CloseIcon,
  DataObjectIcon,
  DeleteIcon,
  DownloadIcon,
  ExpandLessIcon,
  ExpandMoreIcon,
  GridViewIcon,
  HeartFilledIcon,
  HeartIcon,
  KeyIcon,
  ListViewIcon,
  MoreIcon,
  PauseIcon,
  PlayIcon,
  RepeatIcon,
  SearchIcon,
  ShuffleIcon,
  SkipNextIcon,
  SkipPreviousIcon,
  UploadIcon,
  UserIcon,
  VisibilityIcon,
  VisibilityOffIcon,
  VpnKeyIcon
};
