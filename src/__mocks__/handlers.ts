import { rest } from 'msw';
import albumMock from './album.json';
import searchMock from './search.json';
import tokenMock from './token.json';
import trackMock from './track.json';

export default [
  rest.post('https://accounts.spotify.com/api/token', (req, res, ctx) => {
    return res(ctx.delay(500), ctx.json(tokenMock));
  }),
  rest.get('https://api.spotify.com/v1/search*', (req, res, ctx) => {
    return res(ctx.delay(500), ctx.json(searchMock));
  }),
  rest.get('https://api.spotify.com/v1/albums*', (req, res, ctx) => {
    return res(ctx.delay(500), ctx.json(albumMock));
  }),
  rest.get('https://api.spotify.com/v1/tracks*', (req, res, ctx) => {
    return res(ctx.delay(500), ctx.json(trackMock));
  })
];
