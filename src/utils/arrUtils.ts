// prettier-ignore
export const sortBy = <T>(key: keyof T, order = 'asc') => (a: T, b: T) => {
  if (order === 'desc') {
    return a[key] > b[key] ? -1 : a[key] < b[key] ? 1 : 0;
  }

  return a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0;
}

// Fisher-Yates Algorithm
export const shuffle = <T>(array: Array<T>) => {
  let desc = array.length,
    temp,
    asc;

  while (desc) {
    asc = Math.floor(Math.random() * desc--);

    temp = array[desc];
    array[desc] = array[asc];
    array[asc] = temp;
  }

  return array;
};

/**
 * values: '2'
 * currentValue: ['1', '2', '3']
 * valuesWithoutCurrent: ['1', '3']
 * returns: '1' || '3'
 *
 * @param {Array} values
 * @param currentValue
 * @returns randomValue
 */
export const getAnotherRandomValue = <T>(values: Array<T>, currentValue: T): T => {
  const valuesWithoutCurrent = values.filter(val => val !== currentValue);
  const [randomValue] = shuffle<T>(valuesWithoutCurrent);
  return randomValue;
};

/**
 * values: ['1', '2', '3']
 * ------------------------------ *
 * currentValue: '2'
 * returns: '1'
 * ------------------------------ *
 * currentValue: '1'
 * returns: '3'
 *
 * @param {Array} values
 * @param currentValue
 * @returns previousValue
 */
export const getPreviousValue = <T>(values: Array<T>, currentValue: T): T => {
  const currentIdx = values.indexOf(currentValue);

  if (currentIdx - 1 < 0) {
    return values[values.length - 1];
  }

  return values[currentIdx - 1];
};

/**
 * values: ['1', '2', '3']
 * ------------------------------ *
 * currentValue: '2'
 * returns: '3'
 * ------------------------------ *
 * currentValue: '3'
 * returns: '1'
 *
 * @param {Array} values
 * @param currentValue
 * @returns nextId
 */
export const getNextValue = <T>(values: Array<T>, currentValue: T): T => {
  const currentIdx = values.indexOf(currentValue);

  if (currentIdx + 1 > values.length - 1) {
    return values[0];
  }

  return values[currentIdx + 1];
};
