import { format, intervalToDuration, parse } from 'date-fns';

/**
 * Check If Timestamp Is Expired
 *
 * @param {number} timestamp
 * @returns {boolean}
 */
export const isExpired = (timestamp: number): boolean => {
  return new Date(timestamp).getTime() < Date.now();
};

/**
 * Parse ISO Date To Pattern
 *
 * @param {string} strDate ISO Date
 * @param {string} pattern dd/MM/yyyy
 * @returns {string}
 */
export const parseIsoDate = (strDate: string, pattern = 'dd/MM/yyyy'): string => {
  try {
    return format(parse(strDate, 'yyyy-MM-dd', new Date()), pattern);
  } catch {
    return '';
  }
};

/**
 * Interval To Duration
 *
 * @param {number} millis
 * @returns {string} mm:ss
 */
export const millisToTime = (millis: number): string => {
  const { minutes, seconds } = intervalToDuration({ start: 0, end: millis });
  return minutes + ':' + `${seconds}`.padStart(2, '0');
};
