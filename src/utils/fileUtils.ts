/**
 * Create Anchor Element, Then Click On It (On The Fly)
 *
 * @param {File} file
 */
export const downloadFile = (file: File) => {
  const link = document.createElement('a');
  const url = URL.createObjectURL(file);

  link.setAttribute('href', url);
  link.setAttribute('download', file.name);

  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);

  URL.revokeObjectURL(url);
};

/**
 * Async Read File As Text (Through File Reader)
 *
 * @param {File} file
 * @returns {Promise<string>}
 */
export const readFileAsText = (file: File): Promise<string> => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.addEventListener('load', event => {
      if (event.target && event.target.result) {
        resolve(event.target.result as string);
      } else {
        reject(new Error('Not Readable'));
      }
    });

    reader.readAsText(file);
  });
};
