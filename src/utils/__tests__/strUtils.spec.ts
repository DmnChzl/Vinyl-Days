import { generateRandomString, serialize } from '../strUtils';

test('serialize', () => {
  expect(serialize({ accessToken: 'abc123', refreshAccessToken: 'def456' })).toEqual(
    'accessToken=abc123&refreshAccessToken=def456'
  );
});

test('generateRandomString', () => {
  const randomString = generateRandomString(16);
  expect(randomString).toHaveLength(16);
});
