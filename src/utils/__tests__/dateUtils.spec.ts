import { isExpired, millisToTime, parseIsoDate } from '../dateUtils';

describe('isExpired', () => {
  it('Should Be True', () => {
    const beforeMillis = new Date().getTime() - 1000;
    expect(isExpired(beforeMillis)).toBeTruthy();
  });

  it('Should Be False', () => {
    const afterMillis = new Date().getTime() + 1000;
    expect(isExpired(afterMillis)).toBeFalsy();
  });
});

test('parseIsoDate', () => {
  expect(parseIsoDate('1993-05-21')).toEqual('21/05/1993');
});

test('millisToTime', () => {
  expect(millisToTime(65000)).toEqual('1:05');
});
