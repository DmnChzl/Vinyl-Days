import { vi } from 'vitest';
import { downloadFile, readFileAsText } from '../fileUtils';

test('downloadFile', () => {
  const onClickMock = vi.fn();

  global.URL.createObjectURL = vi.fn();
  global.URL.revokeObjectURL = vi.fn();

  // Simulate <a> With <div>
  const elementMock = document.createElement('div');
  elementMock.addEventListener('click', onClickMock);
  vi.spyOn(document, 'createElement').mockReturnValue(elementMock);

  const jsonFile = new File(['{"hello":"world"}'], 'hello_world.json', {
    type: 'application/json'
  });

  downloadFile(jsonFile);
  expect(onClickMock).toHaveBeenCalled();
});

test.skip('readFileAsText', async () => {
  const jsonFile = new File(['{"hello":"world"}'], 'hello_world.json', {
    type: 'application/json'
  });

  const text = await readFileAsText(jsonFile);
  const json = JSON.parse(text);

  expect(json.hello).toEqual('world');
});
