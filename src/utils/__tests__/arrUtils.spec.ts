import { getAnotherRandomValue, getNextValue, getPreviousValue, shuffle, sortBy } from '../arrUtils';

describe('sortBy', () => {
  const albums = [
    { id: 'abc123', name: 'Dial M For Monkey' },
    { id: 'def456', name: 'Black Sands' },
    { id: 'ghi789', name: 'The North Borders' },
    { id: 'jkl012', name: 'Migration' }
  ];

  it("Should Returns 'ASC' Sorted Array", () => {
    const sortedAlbums = albums.sort(sortBy('name', 'asc'));

    expect(sortedAlbums).toHaveLength(4);
    expect(sortedAlbums.map(({ name }) => name).join(', ')).toEqual(
      'Black Sands, Dial M For Monkey, Migration, The North Borders'
    );
  });

  it("Should Returns 'DESC' Sorted Array", () => {
    const sortedAlbums = albums.sort(sortBy('name', 'desc'));

    expect(sortedAlbums).toHaveLength(4);
    expect(sortedAlbums.map(({ name }) => name).join(', ')).toEqual(
      'The North Borders, Migration, Dial M For Monkey, Black Sands'
    );
  });
});

test('shuffle', () => {
  const distinctFibonacci = [0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
  const shuffledFibonacci = shuffle<number>(distinctFibonacci);

  // Check Length
  expect(shuffledFibonacci).toHaveLength(distinctFibonacci.length);

  const sum = (acc, val) => [(acc += val), 0];

  // Check Sum
  expect(shuffledFibonacci.reduce(sum)).toEqual(distinctFibonacci.reduce(sum));
});

test('getAnotherRandomValue', () => {
  expect(getAnotherRandomValue<number>([1, 3], 1)).toEqual(3);
});

test('getPreviousValue', () => {
  const values = [1, 2, 3];
  expect(getPreviousValue<number>(values, 2)).toEqual(1);
  expect(getPreviousValue<number>(values, 1)).toEqual(3);
});

test('getNextValue', () => {
  const values = [1, 2, 3];
  expect(getNextValue<number>(values, 2)).toEqual(3);
  expect(getNextValue<number>(values, 3)).toEqual(1);
});
