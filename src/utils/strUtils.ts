export const serialize = (obj: Record<string, string>): string => {
  const str = [];

  for (const prop in obj) {
    // eslint-disable-next-line
    if (obj.hasOwnProperty(prop)) {
      const encodedObj = `${encodeURIComponent(prop)}=${encodeURIComponent(obj[prop])}`;
      str.push(encodedObj);
    }
  }

  return str.join('&');
};

export const generateRandomString = (length: number): string => {
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let str = '';

  for (let idx = 0; idx < length; idx++) {
    str += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  return str;
};
