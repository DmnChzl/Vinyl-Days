import { useEffect, useReducer } from 'react';

const TIMER_ACTION = {
  START: 'start',
  SET: 'set',
  PAUSE: 'pause',
  STOP: 'stop',
  RESET: 'reset'
};

const TIMER_STATUS = {
  RUNNING: 'running',
  PAUSED: 'paused',
  STOPPED: 'stopped'
};

const TIMER_TYPE = {
  INCREMENTAL: 'incremental',
  DECREMENTAL: 'decremental'
};

interface TimerState {
  status: string;
  time: number;
  timerType: string;
}

interface TimerAction {
  type: string;
  payload?: {
    initialTime?: number;
    newTime?: number;
  };
}

function timerReducer(state: TimerState, action: TimerAction) {
  switch (action.type) {
    case TIMER_ACTION.START: {
      return {
        ...state,
        status: TIMER_STATUS.RUNNING,
        time: state.status === TIMER_STATUS.STOPPED ? (action.payload?.initialTime as number) : state.time
      };
    }

    case TIMER_ACTION.PAUSE: {
      return {
        ...state,
        status: TIMER_STATUS.PAUSED
      };
    }

    case TIMER_ACTION.STOP: {
      return {
        ...state,
        status: TIMER_STATUS.STOPPED
      };
    }

    case TIMER_ACTION.SET: {
      return {
        ...state,
        time: action.payload?.newTime as number
      };
    }

    case TIMER_ACTION.RESET: {
      return {
        ...state,
        status: TIMER_STATUS.STOPPED,
        time: action.payload?.initialTime as number
      };
    }

    default:
      return state;
  }
}

interface TimerProps {
  autoStart: boolean;
  initialStatus: string;
  initialTime: number;
  intervalPeriod: number;
  onTimeUpdate: (time: number) => void;
  onTimeOver: () => void;
  stepperCount: number;
  timerType: string;
  endTime: number;
}

interface TimerValue {
  status: string;
  time: number;
  start: () => void;
  pause: () => void;
  stop: () => void;
  set: (time: number) => void;
  reset: () => void;
}

export const useTimer = ({
  autoStart = false,
  initialStatus = TIMER_STATUS.STOPPED,
  initialTime = 0,
  intervalPeriod = 1000,
  onTimeUpdate,
  onTimeOver,
  stepperCount = 1,
  timerType = TIMER_TYPE.INCREMENTAL,
  endTime
}: Partial<TimerProps> = {}): TimerValue => {
  const [{ status, time }, dispatch] = useReducer(timerReducer, {
    status: initialStatus,
    time: initialTime,
    timerType
  });

  const start = () => {
    dispatch({ type: TIMER_ACTION.START, payload: { initialTime } });
  };

  const pause = () => dispatch({ type: TIMER_ACTION.PAUSE });
  const stop = () => dispatch({ type: TIMER_ACTION.STOP });

  const set = (newTime: number) => {
    dispatch({ type: TIMER_ACTION.SET, payload: { newTime } });
  };

  const reset = () => {
    dispatch({ type: TIMER_ACTION.RESET, payload: { initialTime } });
  };

  useEffect(() => {
    if (autoStart) {
      dispatch({ type: TIMER_ACTION.START, payload: { initialTime } });
    }
  }, [autoStart, initialTime]);

  useEffect(() => {
    if (typeof onTimeUpdate === 'function') {
      onTimeUpdate(time);
    }
  }, [onTimeUpdate, time]);

  useEffect(() => {
    if (endTime && status !== TIMER_STATUS.STOPPED && time >= endTime) {
      dispatch({ type: TIMER_ACTION.STOP });

      if (typeof onTimeOver === 'function') {
        onTimeOver();
      }
    }
  }, [status, time, endTime, onTimeOver]);

  useEffect(() => {
    let intervalId: NodeJS.Timeout | null = null;

    if (status === TIMER_STATUS.RUNNING) {
      intervalId = setInterval(() => {
        dispatch({
          type: TIMER_ACTION.SET,
          payload: {
            newTime: timerType === TIMER_TYPE.INCREMENTAL ? time + stepperCount : time - stepperCount
          }
        });
      }, intervalPeriod);
    } else if (intervalId) {
      clearInterval(intervalId);
    }

    return () => {
      if (intervalId) {
        clearInterval(intervalId);
      }
    };
  }, [status, timerType, time, stepperCount, intervalPeriod]);

  return { status, time, start, pause, stop, set, reset };
};
