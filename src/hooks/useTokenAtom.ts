import { accessTokenAtom, accessTokenExpirationAtom, refreshTokenAtom } from '@/atoms';
import { APP_STATE_KEY } from '@/constants';
import type { AppState } from '@/models/AppState';
import * as StorageService from '@/services/storageService';
import { useAtom } from 'jotai';

interface TokenGetters {
  accessToken: string;
  accessTokenExpiration: number;
  refreshToken: string;
}

interface TokenSetters {
  setAccessToken: (token: string) => void;
  setAccessTokenExpiration: (timestamp: number) => void;
  setRefreshToken: (token: string) => void;
}

export default function useTokenAtom(): [TokenGetters, TokenSetters] {
  const [accessToken, setAccessTokenValue] = useAtom(accessTokenAtom);
  const [accessTokenExpiration, setAccessTokenExpirationValue] = useAtom(accessTokenExpirationAtom);
  const [refreshToken, setRefreshTokenValue] = useAtom(refreshTokenAtom);

  const setAccessToken = (token: string) => {
    StorageService.setItem<AppState>(
      APP_STATE_KEY,
      {
        accessToken: token,
        modifiedAt: Date.now()
      },
      { useLocal: true }
    );
    setAccessTokenValue(token);
  };

  const setAccessTokenExpiration = (timestamp: number) => {
    StorageService.setItem<AppState>(
      APP_STATE_KEY,
      {
        accessTokenExpiration: timestamp,
        modifiedAt: Date.now()
      },
      { useLocal: true }
    );
    setAccessTokenExpirationValue(timestamp);
  };

  const setRefreshToken = (token: string) => {
    StorageService.setItem<AppState>(
      APP_STATE_KEY,
      {
        refreshToken: token,
        modifiedAt: Date.now()
      },
      { useLocal: true }
    );
    setRefreshTokenValue(token);
  };

  return [
    { accessToken, accessTokenExpiration, refreshToken },
    { setAccessToken, setAccessTokenExpiration, setRefreshToken }
  ];
}
