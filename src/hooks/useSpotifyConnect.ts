import * as SpotifyService from '@/services/spotifyService';
import { useEffect } from 'react';
import useClientAtom from './useClientAtom';
import useTokenAtom from './useTokenAtom';

export default function useSpotifyConnect() {
  const [{ clientId, clientSecret }] = useClientAtom();
  const [, { setAccessToken, setAccessTokenExpiration, setRefreshToken }] = useTokenAtom();

  useEffect(() => {
    const grantAuthorizationCode = async () => {
      const urlSearchParams = new URLSearchParams(window.location.search);
      const code = urlSearchParams.get('code') || '';

      if (code && clientId && clientSecret) {
        const client = btoa(`${clientId}:${clientSecret}`);

        const { access_token, expires_in, refresh_token } = await SpotifyService.grantAuthorizationCode(code, client);

        setAccessToken(access_token);
        setAccessTokenExpiration(Date.now() + expires_in * 1000);
        setRefreshToken(refresh_token as string);
      }
    };

    grantAuthorizationCode();
  }, [clientId, clientSecret]);
}
