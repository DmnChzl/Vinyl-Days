import { isSearchedAlbumsEmptyAtom, searchedAlbumsAtom } from '@/atoms';
import { APP_STATE_KEY } from '@/constants';
import type { AppState } from '@/models/AppState';
import type { SearchedAlbum } from '@/models/SearchedAlbum';
import * as StorageService from '@/services/storageService';
import { useAtom, useAtomValue } from 'jotai';

interface PartialSearchedAlbum {
  id: string;
  name: string;
}

interface SearchedAlbumGetters {
  searchedAlbums: SearchedAlbum[];
  isSearchedAlbumsEmpty: boolean;
}

interface SearchedAlbumSetters {
  isSearchedAlbumIncludesId: (id: string) => boolean;
  addSearchedAlbum: (album: PartialSearchedAlbum) => void;
  delSearchedAlbum: (id: string) => void;
}

export default function useSearchedAlbumsAtom(): [SearchedAlbumGetters, SearchedAlbumSetters] {
  const [searchedAlbums, setSearchedAlbumsValue] = useAtom(searchedAlbumsAtom);
  const isSearchedAlbumsEmpty = useAtomValue<boolean>(isSearchedAlbumsEmptyAtom);

  const setSearchedAlbums = (albums: SearchedAlbum[]) => {
    StorageService.setItem<AppState>(
      APP_STATE_KEY,
      {
        searchedAlbums: albums,
        modifiedAt: Date.now()
      },
      { useLocal: true }
    );
    setSearchedAlbumsValue(albums);
  };

  const isSearchedAlbumIncludesId = (id: string): boolean => {
    return searchedAlbums.map(searchedAlbum => searchedAlbum.id).includes(id);
  };

  const addSearchedAlbum = (album: PartialSearchedAlbum) => {
    const now = new Date().getTime();

    if (isSearchedAlbumIncludesId(album.id)) {
      setSearchedAlbums(
        searchedAlbums.map(searchedAlbum =>
          searchedAlbum.id === album.id
            ? {
                ...album,
                count: searchedAlbum.count + 1,
                timestamp: now
              }
            : searchedAlbum
        )
      );
    } else {
      setSearchedAlbums([...searchedAlbums, { ...album, count: 1, timestamp: now }]);
    }
  };

  const delSearchedAlbum = (id: string) => {
    setSearchedAlbums(searchedAlbums.filter(album => album.id !== id));
  };

  return [
    { searchedAlbums, isSearchedAlbumsEmpty },
    { isSearchedAlbumIncludesId, addSearchedAlbum, delSearchedAlbum }
  ];
}
