import {
  accessTokenAtom,
  accessTokenExpirationAtom,
  clientIdAtom,
  clientSecretAtom,
  likedTracksAtom,
  refreshTokenAtom,
  searchedAlbumsAtom
} from '@/atoms';
import { APP_STATE_KEY } from '@/constants';
import type { AppState } from '@/models/AppState';
import * as StorageService from '@/services/storageService';
import { isExpired } from '@/utils/dateUtils';
import { useSetAtom } from 'jotai';
import { useEffect } from 'react';
import useToggle from './useToggle';

export default function useAppStateEffect(): boolean {
  const [initialized, initializedOn] = useToggle(); // false

  const setClientId = useSetAtom(clientIdAtom);
  const setClientSecret = useSetAtom(clientSecretAtom);
  const setAccessToken = useSetAtom(accessTokenAtom);
  const setAccessTokenExpiration = useSetAtom(accessTokenExpirationAtom);
  const setRefreshToken = useSetAtom(refreshTokenAtom);
  const setSearchedAlbums = useSetAtom(searchedAlbumsAtom);
  const setLikedTracks = useSetAtom(likedTracksAtom);

  useEffect(() => {
    const appState = StorageService.getItem<AppState>(APP_STATE_KEY, {
      useLocal: true
    }) as Partial<AppState>;
    const now = new Date().getTime();

    if (Object.entries(appState).length > 0) {
      if (appState.clientId) setClientId(appState.clientId);
      if (appState.clientSecret) setClientSecret(appState.clientSecret);

      if (appState.accessToken && appState.accessTokenExpiration && !isExpired(appState.accessTokenExpiration)) {
        setAccessToken(appState.accessToken);
        setAccessTokenExpiration(appState.accessTokenExpiration);
      }

      if (appState.refreshToken) setRefreshToken(appState.refreshToken);

      if (appState.searchedAlbums && appState.searchedAlbums.length > 0) {
        setSearchedAlbums(appState.searchedAlbums);
      }

      if (appState.likedTracks && appState.likedTracks.length > 0) {
        setLikedTracks(appState.likedTracks);
      }
    } else {
      StorageService.setItem<AppState>(
        APP_STATE_KEY,
        {
          createdAt: now,
          modifiedAt: now
        },
        { useLocal: true }
      );
    }

    const clientId = import.meta.env['VITE_CLIENT_ID'];
    if (clientId) {
      StorageService.setItem<AppState>(
        APP_STATE_KEY,
        {
          clientId,
          modifiedAt: now
        },
        { useLocal: true }
      );
      setClientId(clientId);
    }

    const clientSecret = import.meta.env['VITE_CLIENT_SECRET'];
    if (clientSecret) {
      StorageService.setItem<AppState>(
        APP_STATE_KEY,
        {
          clientSecret,
          modifiedAt: now
        },
        { useLocal: true }
      );
      setClientSecret(clientSecret);
    }

    initializedOn();
  }, []);

  return initialized;
}
