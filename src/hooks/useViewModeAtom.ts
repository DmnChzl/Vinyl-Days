import { isGridViewAtom, isListViewAtom, viewModeAtom } from '@/atoms';
import { VIEW_TYPE } from '@/constants';
import { useAtom, useAtomValue } from 'jotai';

interface ViewModeGetters {
  viewMode: string;
  isGridView: boolean;
  isListView: boolean;
}

interface ViewModeSetters {
  switchGridView: () => void;
  switchListView: () => void;
  toggleViewMode: () => void;
}

export default function useViewModeAtom(): [ViewModeGetters, ViewModeSetters] {
  const [viewMode, setViewMode] = useAtom(viewModeAtom);

  const isGridView = useAtomValue(isGridViewAtom);
  const isListView = useAtomValue(isListViewAtom);

  const switchGridView = () => setViewMode(VIEW_TYPE.GRID);
  const switchListView = () => setViewMode(VIEW_TYPE.LIST);

  const toggleViewMode = () => (isGridView ? switchListView() : switchGridView());

  return [
    {
      viewMode,
      isGridView,
      isListView
    },
    {
      switchGridView,
      switchListView,
      toggleViewMode
    }
  ];
}
