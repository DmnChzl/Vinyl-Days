import { ChangeEvent, useState } from 'react';

export default function useField(
  initialState: string
): [string, (event: ChangeEvent<HTMLInputElement>) => void, () => void] {
  const [value, setValue] = useState(initialState);

  return [value, (event: ChangeEvent<HTMLInputElement>) => setValue(event.target.value), () => setValue(initialState)];
}
