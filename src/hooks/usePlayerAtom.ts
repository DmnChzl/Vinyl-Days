import { deviceIdAtom, playerAtom, playerPausedAtom, playerPositionAtom } from '@/atoms';
import { useAtomValue } from 'jotai';

interface PlayerValue {
  player: Spotify.Player | undefined;
  isPlaying: boolean;
  position: number;
  deviceId: string;
}

export default function usePlayerAtom(): PlayerValue {
  const player = useAtomValue(playerAtom);
  const deviceId = useAtomValue(deviceIdAtom);
  const playerPaused = useAtomValue(playerPausedAtom);
  const playerPosition = useAtomValue(playerPositionAtom);

  return {
    player,
    deviceId,
    isPlaying: !playerPaused,
    position: playerPosition
  };
}
