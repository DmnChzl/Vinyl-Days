import { deviceIdAtom, playerAtom, playerPausedAtom, playerPositionAtom } from '@/atoms';
import { APP_NAME } from '@/constants';
import * as SpotifyService from '@/services/spotifyService';
import { useAtom, useSetAtom } from 'jotai';
import { useEffect } from 'react';
import useTokenAtom from './useTokenAtom';

const SPOTIFY_PLAYER_SDK = 'https://sdk.scdn.co/spotify-player.js';

export default function useSpotifyPlayer() {
  const [{ accessToken, refreshToken }] = useTokenAtom();
  const [player, setPlayer] = useAtom(playerAtom);
  const setDeviceId = useSetAtom(deviceIdAtom);
  const setPlayerPaused = useSetAtom(playerPausedAtom);
  const setPlayerPosition = useSetAtom(playerPositionAtom);

  useEffect(() => {
    if (refreshToken && accessToken) {
      // Append Script (By ID), If Not Exists
      if (!document.getElementById('spotifyPlayerSdk')) {
        const script = document.createElement('script');
        script.setAttribute('id', 'spotifyPlayerSdk');
        script.setAttribute('src', SPOTIFY_PLAYER_SDK);
        document.head.appendChild(script);
      }

      window.onSpotifyWebPlaybackSDKReady = () => {
        const newPlayer = new window.Spotify.Player({
          name: APP_NAME,
          getOAuthToken: callback => callback(accessToken),
          volume: 1.0 // 0.5
        });

        setPlayer(newPlayer);

        newPlayer.addListener('ready', async ({ device_id }: { device_id: string }) => {
          await SpotifyService.setDevicePlayer(device_id, accessToken);
          setDeviceId(device_id);
        });

        newPlayer.connect();

        newPlayer.addListener('player_state_changed', (state: { paused: boolean; position: number }) => {
          if (!state) return;

          setPlayerPaused(state.paused);
          setPlayerPosition(state.position);
        });
      };
    }

    return () => {
      if (player) player.disconnect();
    };
  }, [accessToken, refreshToken, player]);
}
