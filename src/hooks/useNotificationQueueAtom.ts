import { isNotificationQueueEmptyAtom, notificationQueueAtom } from '@/atoms';
import { NOTIFICATION_THEME } from '@/constants';
import type { Notification } from '@/models/Notification';
import { generateRandomString } from '@/utils/strUtils';
import { useAtom, useAtomValue } from 'jotai';

interface PartialNotification {
  title: string;
  subTitle?: string;
  theme?: string;
}

interface NotificationQueueGetters {
  notificationQueue: Notification[];
  isNotificationQueueEmpty: boolean;
}

interface NotificationQueueSetters {
  addNotification: (notification: PartialNotification, delay?: number) => void;
  delNotification: (id: string) => void;
  resetNotificationQueue: () => void;
}

export default function useNotificationQueueAtom(): [NotificationQueueGetters, NotificationQueueSetters] {
  const [notificationQueue, setNotificationQueue] = useAtom(notificationQueueAtom);
  const isNotificationQueueEmpty = useAtomValue(isNotificationQueueEmptyAtom);

  const delNotification = (id: string) => {
    setNotificationQueue(queue => queue.filter(notification => notification.id !== id));
  };

  const addNotification = (notification: PartialNotification, delay = 2500) => {
    if (notification.title) {
      const id = generateRandomString(8);

      setNotificationQueue([{ theme: NOTIFICATION_THEME.LIGHT, ...notification, id }, ...notificationQueue]);

      const timer = setTimeout(() => {
        delNotification(id);
        clearTimeout(timer);
      }, delay);
    }
  };

  const resetNotificationQueue = () => setNotificationQueue([]);

  return [
    { notificationQueue, isNotificationQueueEmpty },
    { addNotification, delNotification, resetNotificationQueue }
  ];
}
