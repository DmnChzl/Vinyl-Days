import { isLikedTracksEmptyAtom, likedTracksAtom } from '@/atoms';
import { APP_STATE_KEY } from '@/constants';
import type { AppState } from '@/models/AppState';
import type { LikedTrack } from '@/models/LikedTrack';
import * as StorageService from '@/services/storageService';
import { useAtom, useAtomValue } from 'jotai';

interface LikedTracksGetters {
  likedTracks: LikedTrack[];
  isLikedTracksEmpty: boolean;
}

interface LikedTracksSetters {
  isLikedTracksIncludesId: (id: string) => boolean;
  addLikedTrack: (track: LikedTrack) => void;
  delLikedTrack: (id: string) => void;
}

export default function useLikedTracksAtom(): [LikedTracksGetters, LikedTracksSetters] {
  const [likedTracks, setLikedTracksValue] = useAtom(likedTracksAtom);
  const isLikedTracksEmpty = useAtomValue<boolean>(isLikedTracksEmptyAtom);

  const setLikedTracks = (tracks: LikedTrack[]) => {
    StorageService.setItem<AppState>(
      APP_STATE_KEY,
      {
        likedTracks: tracks,
        modifiedAt: Date.now()
      },
      { useLocal: true }
    );
    setLikedTracksValue(tracks);
  };

  const isLikedTracksIncludesId = (id: string): boolean => {
    return likedTracks.map(likedTrack => likedTrack.id).includes(id);
  };

  const addLikedTrack = (track: LikedTrack) => {
    setLikedTracks([...likedTracks, { ...track, timestamp: Date.now() }]);
  };

  const delLikedTrack = (id: string) => {
    setLikedTracks(likedTracks.filter(track => track.id !== id));
  };

  return [
    { likedTracks, isLikedTracksEmpty },
    { isLikedTracksIncludesId, addLikedTrack, delLikedTrack }
  ];
}
