import { notificationQueueAtom } from '@/atoms';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import { useSetAtom } from 'jotai';
import { useEffect } from 'react';
import { vi } from 'vitest';
import useNotificationQueueAtom from '../useNotificationQueueAtom';

describe('useNotificationQueueAtom', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });

  afterEach(() => {
    cleanup();
    vi.restoreAllMocks();
  });

  it("Should Add 'notification'", () => {
    const Wrapper = ({ notification }: { id: string; title: string }) => {
      const [{ notificationQueue }, { addNotification }] = useNotificationQueueAtom();

      return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <button onClick={() => addNotification(notification)}>Add Notification</button>

          <ul>
            {notificationQueue.map((notication, idx) => (
              <li key={idx}>{notification.title}</li>
            ))}
          </ul>
        </div>
      );
    };

    render(
      <Wrapper
        notification={{
          id: 'def456',
          title: 'Another Notification'
        }}
      />
    );

    fireEvent.click(screen.getByRole('button', { name: 'Add Notification' }));
    expect(screen.getByText('Another Notification')).toBeInTheDocument();
  });

  it("Should Del 'notification'", () => {
    const Wrapper = ({ notification }: { id: string; title: string }) => {
      const [{ notificationQueue }, { delNotification }] = useNotificationQueueAtom();
      const setNotificationQueue = useSetAtom(notificationQueueAtom);

      useEffect(() => {
        setNotificationQueue([
          {
            id: 'abc123',
            title: 'Default Notification'
          }
        ]);
      }, []);

      return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <button onClick={() => delNotification(notification.id)}>Del Notification</button>

          <ul>
            {notificationQueue.map((notication, idx) => (
              <li key={idx}>{notification.title}</li>
            ))}
          </ul>
        </div>
      );
    };

    render(
      <Wrapper
        notification={{
          id: 'abc123',
          title: 'Default Notification'
        }}
      />
    );

    fireEvent.click(screen.getByRole('button', { name: 'Del Notification' }));
    expect(screen.queryByText('Default Notification')).not.toBeInTheDocument();
  });
});
