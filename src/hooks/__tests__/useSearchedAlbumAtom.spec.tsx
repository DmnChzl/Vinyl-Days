import StorageMock from '@/__mocks__/StorageMock';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import useSearchedAlbumsAtom from '../useSearchedAlbumsAtom';

describe('useSearchedAlbumsAtom', () => {
  const Wrapper = ({ searchedAlbum }: { id: string; name: string }) => {
    const [{ searchedAlbums }, { addSearchedAlbum, delSearchedAlbum }] = useSearchedAlbumsAtom();

    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ display: 'flex' }}>
          <button onClick={() => addSearchedAlbum(searchedAlbum)}>Add Album</button>
          <button onClick={() => delSearchedAlbum(searchedAlbum.id)}>Del Album</button>
        </div>

        <ul>
          {searchedAlbums.map((searchedAlbum, idx) => (
            <li key={idx}>{searchedAlbum.name}</li>
          ))}
        </ul>
      </div>
    );
  };

  beforeAll(() => (global.localStorage = new StorageMock()));
  afterEach(cleanup);
  afterAll(() => global.localStorage.clear());

  it("Should Add 'searchedAlbum'", () => {
    render(
      <Wrapper
        searchedAlbum={{
          id: 'def456',
          name: 'My Album'
        }}
      />
    );

    fireEvent.click(screen.getByRole('button', { name: 'Add Album' }));

    expect(screen.getByText('My Album')).toBeInTheDocument();

    const item = global.localStorage.getItem('vinyl_days_app');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.searchedAlbums).toHaveLength(1);
  });

  it("Should Del 'searchedAlbum'", () => {
    render(
      <Wrapper
        searchedAlbum={{
          id: 'def456',
          name: 'My Album'
        }}
      />
    );

    fireEvent.click(screen.getByRole('button', { name: 'Del Album' }));

    expect(screen.queryByText('My Album')).not.toBeInTheDocument();

    const item = global.localStorage.getItem('vinyl_days_app');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.searchedAlbums).toHaveLength(0);
  });
});
