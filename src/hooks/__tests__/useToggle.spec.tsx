import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import useToggle from '../useToggle';

describe('useToggle', () => {
  const Wrapper = ({ initialState }) => {
    const [value, switchOn, switchOff] = useToggle(initialState);

    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <span style={{ display: value ? 'block' : 'hidden' }}>Value: +1</span>
        <span style={{ display: value ? 'hidden' : 'block' }}>Value: -1</span>

        <div style={{ display: 'flex' }}>
          <button onClick={switchOn}>Switch On</button>
          <button onClick={switchOff}>Switch Off</button>
        </div>
      </div>
    );
  };

  afterEach(cleanup);

  it('Should Toggle On', () => {
    render(<Wrapper initialState={false} />);
    fireEvent.click(screen.getByRole('button', { name: 'Switch On' }));
    expect(screen.getByText('Value: +1')).toBeVisible();
  });

  it('Should Toggle Off', () => {
    render(<Wrapper initialState={true} />);
    fireEvent.click(screen.getByRole('button', { name: 'Switch Off' }));
    expect(screen.getByText('Value: -1')).toBeVisible();
  });
});
