import { deviceIdAtom, playerPausedAtom, playerPositionAtom } from '@/atoms';
import { cleanup, render, screen } from '@testing-library/react';
import { useSetAtom } from 'jotai';
import { useEffect } from 'react';
import usePlayerAtom from '../usePlayerAtom';

describe('usePlayerAtom', () => {
  afterEach(cleanup);

  it('Should Return Value', () => {
    const Wrapper = () => {
      const { deviceId, isPlaying, position } = usePlayerAtom();

      const setDeviceId = useSetAtom(deviceIdAtom);
      const setPlayerPaused = useSetAtom(playerPausedAtom);
      const setPlayerPosition = useSetAtom(playerPositionAtom);

      useEffect(() => {
        setDeviceId('abc123');
        setPlayerPaused(false);
        setPlayerPosition(30000);
      }, []);

      return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <span>Device ID: {deviceId}</span>
          <span>Status: {isPlaying ? 'Running' : 'Paused'}</span>
          <span>Time: {position / 1000}s</span>
        </div>
      );
    };

    render(<Wrapper />);

    expect(screen.getByText('Device ID: abc123')).toBeInTheDocument();
    expect(screen.getByText('Status: Running')).toBeInTheDocument();
    expect(screen.getByText('Time: 30s')).toBeInTheDocument();
  });
});
