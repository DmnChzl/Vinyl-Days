import StorageMock from '@/__mocks__/StorageMock';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import { ChangeEvent } from 'react';
import useClientAtom from '../useClientAtom';

describe('useClientAtom', () => {
  beforeAll(() => (global.localStorage = new StorageMock()));
  afterEach(cleanup);
  afterAll(() => global.localStorage.clear());

  it("Should Updates 'clientId'", () => {
    const Wrapper = () => {
      const [{ clientId }, { setClientId }] = useClientAtom();

      return (
        <input
          defaultValue={clientId}
          onChange={(e: ChangeEvent<HTMLInputElement>) => {
            setClientId(e.target.value);
          }}
        />
      );
    };

    render(<Wrapper />);

    const textBox = screen.getByRole('textbox');
    fireEvent.change(textBox, { target: { value: 'abc123' } });

    const item = global.localStorage.getItem('vinyl_days_app');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.clientId).toEqual('abc123');
  });

  it("Should Updates 'clientSecret'", () => {
    const Wrapper = () => {
      const [{ clientSecret }, { setClientSecret }] = useClientAtom();
      return (
        <input
          defaultValue={clientSecret}
          onChange={(e: ChangeEvent<HTMLInputElement>) => {
            setClientSecret(e.target.value);
          }}
        />
      );
    };

    render(<Wrapper />);

    const textBox = screen.getByRole('textbox');
    fireEvent.change(textBox, { target: { value: 'def456' } });

    const item = global.localStorage.getItem('vinyl_days_app');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.clientSecret).toEqual('def456');
  });
});
