import StorageMock from '@/__mocks__/StorageMock';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import { ChangeEvent } from 'react';
import useTokenAtom from '../useTokenAtom';

describe('useTokenAtom', () => {
  beforeAll(() => (global.localStorage = new StorageMock()));
  afterEach(cleanup);
  afterAll(() => global.localStorage.clear());

  it("Should Updates 'accessToken'", () => {
    const Wrapper = () => {
      const [{ accessToken }, { setAccessToken }] = useTokenAtom();

      return (
        <input
          defaultValue={accessToken}
          onChange={(e: ChangeEvent<HTMLInputElement>) => {
            setAccessToken(e.target.value);
          }}
        />
      );
    };

    render(<Wrapper />);

    const textBox = screen.getByRole('textbox');
    fireEvent.change(textBox, { target: { value: 'abc123' } });

    const item = global.localStorage.getItem('vinyl_days_app');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.accessToken).toEqual('abc123');
  });

  it("Should Updates 'refreshToken'", () => {
    const Wrapper = () => {
      const [{ refreshToken }, { setRefreshToken }] = useTokenAtom();
      return (
        <input
          defaultValue={refreshToken}
          onChange={(e: ChangeEvent<HTMLInputElement>) => {
            setRefreshToken(e.target.value);
          }}
        />
      );
    };

    render(<Wrapper />);

    const textBox = screen.getByRole('textbox');
    fireEvent.change(textBox, { target: { value: 'def456' } });

    const item = global.localStorage.getItem('vinyl_days_app');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.refreshToken).toEqual('def456');
  });
});
