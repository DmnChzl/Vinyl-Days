import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import useField from '../useField';

describe('useField', () => {
  const Wrapper = ({ initialState }) => {
    const [field, setField] = useField(initialState);
    return <input defaultValue={field} onChange={setField} />;
  };

  afterEach(cleanup);

  it("Should Renders 'initialState'", () => {
    render(<Wrapper initialState="Lorem Ipsum" />);
    expect(screen.getByRole('textbox')).toHaveValue('Lorem Ipsum');
  });

  it("Should Updates 'initialState'", () => {
    render(<Wrapper initialState="Lorem Ipsum" />);
    const textBox = screen.getByRole('textbox');
    fireEvent.change(textBox, { target: { value: 'Dolor Sit Amet' } });
    expect(textBox).toHaveValue('Dolor Sit Amet');
  });
});
