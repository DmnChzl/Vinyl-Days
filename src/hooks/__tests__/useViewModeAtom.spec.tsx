import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import useViewModeAtom from '../useViewModeAtom';

describe('useViewModeAtom', () => {
  const Wrapper = () => {
    const [{ viewMode }, { switchGridView, switchListView }] = useViewModeAtom();

    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <span>{viewMode === 'grid' ? 'isGridView' : 'isListView'}</span>

        <div style={{ display: 'flex' }}>
          <button onClick={switchGridView}>Switch Grid</button>
          <button onClick={switchListView}>Switch List</button>
        </div>
      </div>
    );
  };

  afterEach(cleanup);

  it("Should Set 'viewMode' To 'GRID'", () => {
    render(<Wrapper />);
    fireEvent.click(screen.getByRole('button', { name: 'Switch Grid' }));
    expect(screen.getByText('isGridView')).toBeInTheDocument();
  });

  it("Should Set 'viewMode' To 'LIST'", () => {
    render(<Wrapper />);
    fireEvent.click(screen.getByRole('button', { name: 'Switch List' }));
    expect(screen.getByText('isListView')).toBeInTheDocument();
  });
});
