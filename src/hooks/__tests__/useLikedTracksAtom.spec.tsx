import StorageMock from '@/__mocks__/StorageMock';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import useLikedTracksAtom from '../useLikedTracksAtom';

describe('useLikedTracksAtom', () => {
  const Wrapper = ({ likedTrack }: { id: string; name: string; artist: string; coverUrl: string }) => {
    const [{ likedTracks }, { addLikedTrack, delLikedTrack }] = useLikedTracksAtom();

    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ display: 'flex' }}>
          <button onClick={() => addLikedTrack(likedTrack)}>Add Track</button>
          <button onClick={() => delLikedTrack(likedTrack.id)}>Del Track</button>
        </div>

        <ul>
          {likedTracks.map((likedTrack, idx) => (
            <li key={idx}>{likedTrack.name}</li>
          ))}
        </ul>
      </div>
    );
  };

  beforeAll(() => (global.localStorage = new StorageMock()));
  afterEach(cleanup);
  afterAll(() => global.localStorage.clear());

  it("Should Add 'likedTrack'", () => {
    render(
      <Wrapper
        likedTrack={{
          id: 'abc123',
          name: 'My Track',
          artist: 'My Artist',
          coverUrl: 'https://i.scdn.co/image'
        }}
      />
    );

    fireEvent.click(screen.getByRole('button', { name: 'Add Track' }));

    expect(screen.getByText('My Track')).toBeInTheDocument();

    const item = global.localStorage.getItem('vinyl_days_app');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.likedTracks).toHaveLength(1);
  });

  it("Should Del 'likedTrack'", () => {
    render(
      <Wrapper
        likedTrack={{
          id: 'abc123',
          name: 'My Track',
          artist: 'My Artist',
          coverUrl: 'https://i.scdn.co/image'
        }}
      />
    );

    fireEvent.click(screen.getByRole('button', { name: 'Del Track' }));

    expect(screen.queryByText('My Track')).not.toBeInTheDocument();

    const item = global.localStorage.getItem('vinyl_days_app');
    const parsedItem = JSON.parse(item);

    expect(parsedItem.likedTracks).toHaveLength(0);
  });
});
