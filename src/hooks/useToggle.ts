import { useState } from 'react';

export default function useToggle(initialState = false): [boolean, () => void, () => void, () => void] {
  const [value, setValue] = useState(initialState);

  return [value, () => setValue(true), () => setValue(false), () => setValue(val => !val)];
}
