import { clientIdAtom, clientSecretAtom } from '@/atoms';
import { APP_STATE_KEY } from '@/constants';
import type { AppState } from '@/models/AppState';
import * as StorageService from '@/services/storageService';
import { useAtom } from 'jotai';

interface ClientGetters {
  clientId: string;
  clientSecret: string;
}

interface ClientSetters {
  setClientId: (id: string) => void;
  setClientSecret: (secret: string) => void;
}

export default function useClientAtom(): [ClientGetters, ClientSetters] {
  const [clientId, setClientIdValue] = useAtom(clientIdAtom);
  const [clientSecret, setClientSecretValue] = useAtom(clientSecretAtom);

  const setClientId = (id: string) => {
    StorageService.setItem<AppState>(
      APP_STATE_KEY,
      {
        clientId: id,
        modifiedAt: Date.now()
      },
      { useLocal: true }
    );
    setClientIdValue(id);
  };

  const setClientSecret = (secret: string) => {
    StorageService.setItem<AppState>(
      APP_STATE_KEY,
      {
        clientSecret: secret,
        modifiedAt: Date.now()
      },
      { useLocal: true }
    );
    setClientSecretValue(secret);
  };

  return [
    {
      clientId,
      clientSecret
    },
    {
      setClientId,
      setClientSecret
    }
  ];
}
