module.exports = {
  plugins: ['prettier-plugin-tailwindcss'],
  arrowParens: 'avoid',
  bracketSameLine: true,
  printWidth: 120,
  singleQuote: true,
  trailingComma: 'none'
};
