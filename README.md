# Vinyl Days

> 1.0.0

I made a vinyl / music album search engine (#ForTheLulz), based on the **Spotify** Web API and [Jotai](https://jotai.org) state management.

## Process

Repository:

```
git clone https://gitlab.com/DmnChzl/Vinyl-Days.git
```

Install:

```
pnpm install
```

Dev:

```
pnpm run dev
```

Build:

```
pnpm run build
```

Enjoy! 👌

## Preview

### Mobile

<img src="./screenshots/mobile_dashboard.webp" width="330" height="435" alt="Mobile DashBoard">
<img src="./screenshots/mobile_current_album.webp" width="330" height="435" alt="Mobile Current Album">
<img src="./screenshots/mobile_now_playing.webp" width="330" height="435" alt="Mobile Now Playing">

### DeskTop

<img src="./screenshots/desktop_dashboard.webp" width="775" height="435" alt="DeskTop DashBoard">
<img src="./screenshots/desktop_current_album.webp" width="775" height="435" alt="DeskTop Current Album">
<img src="./screenshots/desktop_now_playing.webp" width="775" height="435" alt="DeskTop Now Playing">

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```